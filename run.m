/////// functionalities for theta and isogenies;
AttachSpec("vendor/avisogenies/src/AVI.spec");
SetVerbose("AVIsogenies",7);
SetIndent(2);


// functions to compute Galoisdescent of quartics and twists
load "descent/galoisdescent.m";

// load "obstruction/libseq.m";
// load "obstruction/reconstruction.m"; // galois descent for quartic curves
load "isogenies/conversion.m"; // elliptic to theta
load "isogenies/isogenies.m"; // compute isogenies
load "isogenies/modular.m"; // compute modular forms

// function on elliptic curves
Attach("elliptic/ecend.m");
load "elliptic/ec-min-order.m";

// functionalities for the lattices
AttachSpec("lat/lat.spec");
load "lat/unimod.m"; //This package deals with lattices over a non-maximal order.

load "diagonalization.m"; 

// here are functionality to compute the action on the l-torsion
load "torsion-ec-av.m";
load "synthesis.m";

function Defect0Curves(q,g)
  disc:=DiscriminantDefect0EllipticCurve(q);
  E,kernels:=GenerateKernels(disc,g : q:=q);
  curves:=AllCurves(kernels: filter:=1+q+g*Floor(2*Sqrt(q)));
  return E, kernels, curves;
end function;

/*

// here one has all the functionalities to generate the lattices
AttachSpec("lat/lat.spec");
load "lat/unimod.m"; //This package generates lattices over a non-maximal order.
load "diagonalization.m"; //needs this first
load "FromLatticeToAV.m";
Attach("ecend.m");
load "NonMaximalOrder.m";

// here are functionality to compute the action on the l-torsion
load "torsion-ec-av.m";
load "find-ec.m";
load "synthesis.m";
*/
