// some more genus 2 examples
// Second example in genus 2: this one has defect 2
g:=2; q:=71; disc:=-59; E,kernels:=GenerateKernels(disc,g : q:=q, Maxell:=40);
i:=12; thetas:=IsogeniesFromKernels([kernels[i]]);
chi10 := Genus2Chi10(thetas[i], kernels[i]); chi10;
theta_analytic:=GetAnalyticThetaNullPoint(thetas[i]);
coord:=theta_analytic`coord; DDg:=Universe(coord);
Genus2Reconstruction(coord,DDg,q : NumberOfPoints:=1+q+g*Floor(2*Sqrt(q))-2);
AllTwists(thetas[i] : NumberOfPoints:=1+q+g*Floor(2*Sqrt(q))-2);
AllCurves(kernels); // All at once



// third example
g:=2; q:=127; disc:=-67; E,kernels:=GenerateKernels(disc,g : q:=q);
AllCurves(kernels);


// fourth example in genus 2:
g:=2; q:=107; disc:=-7*4; E,kernels:=GenerateKernels(disc,g : q:=q); // for the second free lattice one cannot find an ell
i:=1; thetas:=IsogeniesFromKernels([kernels[i]]); AllTwists(thetas[1]);
AllCurves(kernels: filter:=false); // All at once:


// big isogeny class in genus 4:
g:=4; q:=89; disc:=-2^5; E,kernels:=GenerateKernels(disc,g : q:=q);
time thetas:=IsogeniesFromKernels(kernels);
[Genus4Igusa(thetas[i], kernels[i]): i in [1..#kernels]];
