load "run.m";

// quick example for sanity check
print "***************Examples with g=2 and Disc=-19**********";
// SetOutputFile("out/g2D19.m": Overwrite:=true);
g:=2; q:=61; disc:=-19; E,kernels:=GenerateKernels(disc,g : q:=q);
AllCurves(kernels); // all at once:
// UnsetOutputFile();

print "***************Examples with -40**********";
// SetOutputFile("out/g3D40.m": Overwrite:=true);
q0:=131; g:=3; disc:=-40; E,kernels:=GenerateKernels(disc,g : q:=q0);
AllCurves(kernels: filter:=1+q0+3*Floor(2*Sqrt(q0)));
// UnsetOutputFile();

print "***************Examples with -27**********";
// SetOutputFile("out/g3D27.m": Overwrite:=true);
q0:=97; g:=3; disc:=-27; E,kernels:=GenerateKernels(disc,g : q:=q0);
AllCurves(kernels: filter:=1+q0+3*Floor(2*Sqrt(q0)));
// UnsetOutputFile();

// UnsetOutputFile();
// print "***************Examples with -43**********";
SetOutputFile("out/g3D43.m": Overwrite:=true);
q0:=10313; g:=3; disc:=-43; E,kernels:=GenerateKernels(disc,g : q:=q0);
AllCurves(kernels: filter:=1+q0+3*Floor(2*Sqrt(q0)));
// UnsetOutputFile();

