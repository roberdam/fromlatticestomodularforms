/* small exemples to test during refactorisation */
load "run.m";
SetDebugOnError(true);

g:=2; q:=61; disc:=-19; E,kernels:=GenerateKernels(disc,g : q:=q); // There is only one indecomposable principally polarized abelian threefold B in the isogeny class. 
allcurves1:=AllCurves(kernels);
// AllCurves(kernels: l2:=true); // All at once

g:=2; q:=125; disc:=-2^4; E,kernels:=GenerateKernels(disc,g : q:=q);
AllCurves(kernels: filter:=1+q+g*Floor(2*Sqrt(q))); // All at once but output only the curves with the number of rational points indicated in the filter


EGlob:=EllipticCurve([0,0,1,-2*19,(19^2-1) div 4]); //CM curve over Q with disc=-19
FF:=GF(17); E:=WeierstrassModel(ChangeRing(EGlob,FF)); // End(E)=Z[pi]=max order
kernels:=EllipticCurveToKernels(E,3);
allcurves2:=AllCurves(kernels: filter:=false); // All at once
