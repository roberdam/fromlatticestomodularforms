load "run.m";
SetDebugOnError(true);

/////////////////////////////////////////////////////////////////////////
// Functionalities on Lattices
/////////////////////////////////////////////////////////////////////

/*Given an imaginary quadratic order R or its discriminant and an integer g 
it returns the list of all (by default otherwise projective) unimodular 
(indecomposable by default) positive definite hermitian module of rank g over R with their 
automorphism group and the number of orthogonal components.
The last two arguments gives the isomorphism class of the lattice.
The list is sort by first the free lattices, then by groups of isomorphic lattices and 
finally the decomposable ones by number of components */

LList:=OrderToLattices(-19,4); #LList;
LList:=OrderToLattices(-40,3 : Indecomposable:=false, PrintList:=true); #LList;
LList:=OrderToLattices(-27,3); #LList;
LList:=OrderToLattices(-27,3 : Projective:=true); #LList;

//////////////////////////////////////////////////////////////////////////
// Functionalities to go from Lattices to abelian varieties
///////////////////////////////////////////////////////////////////////

/*Given the discriminant of an imaginary quadratic order R, an integer g>1 and
 and integer N  returns 
 returns an elliptic curve with End(E)=R=Z[pi] over Fq with q<qmax (by default q odd) if any
 and a list of couple (Ker,ell) with Ker the kernel of a (ell,...,ell) 
isogeny E^g->A for all principally polarized A in the isogeny class of E^g.
One can also fix a q if we want an elliptic curve over Fq 
If ell=0 it means that it failed to do so.
One can ask ell to be odd (by default) ; one can ask for the ones which corresponds to
projective modules only (false by default), the varieties to be indecomposable (by default).
The program looks at ell smaller than a bound Maxell (default 50)
*/

E,kernels:=GenerateKernels(-19,4);
E,kernels:=GenerateKernels(-40,2 : odd:=false,q:=131);
E,kernels:=GenerateKernels(-27,3 : Indecomposable:=false);
E,kernels:=GenerateKernels(-27,3);

/*Given an elliptic curve E with endomorphism ring R:=Z[pi] and an integer 
g it returns a list of couple (Ker,ell) with Ker the kernel of a (ell,...,ell) 
isogeny E^g->A for all principally polarized A in the isogeny class of E^g.
If ell=0 it means that it failed to do so. 
One can ask ell to be odd (by default) ; one can ask for the ones which corresponds to
projective modules only (false by default), the varieties to be indecomposable (by default).
The program looks at ell smaller than a bound Maxell (default 50)
*/

EGlob:=EllipticCurve([0,0,1,-2*19,(19^2-1) div 4]);
FF:=GF(17); E:=WeierstrassModel(ChangeRing(EGlob,FF));
kernels:=EllipticCurveToKernels(E,3);


/*Given a Z-basis of a R=Z[pi]-lattice (ZB,H) and an elliptic curve E/Fq 
returns the kernel of a (ell,...,ell) isogeny E^g->A when it exists. 
The parameter Odd controls if we want ell to be odd or not. We also impose
ell to be prime to q.
The parameter Maxell (default 50) is a bound for the largest ell we allow to test.*/

K:=QuadraticField(-43); O:=MaximalOrder(K);
LList:=OrderToLattices(O,3); L:=LList[1];
E:=EllipticCurve([GF(11)!5,10]);
kernel:=LatticeToKernel(L,E);

/////////////////////////////////////////////////////////////////////////////
// Functionalities with theta
////////////////////////////////////////////////////////////////////////////

//////////// First exemple in genus 2 ////////////////////////
g:=2; q:=61; disc:=DiscriminantDefect0EllipticCurve(q); //disc=-19
E,kernels:=GenerateKernels(disc,g : q:=q); // There is only one indecomposable principally polarized abelian threefold B in the isogeny class. 
thetas:=IsogeniesFromKernels(kernels); // IsogeniesFromKernels computes a modular lift of the theta null point on B
chi10 := Genus2Chi10(thetas[1], kernels[1]); // here we comute the value of the modular form chi10 at a modular lift
curves := AllTwists(thetas[1]); //output all twists of the curve defined by the theta null point 1
AllCurves(kernels); // all at once:
// [IsPower(chi10/(2^12*Discriminant(C)),10) : C in curves];

  // Aside: we want to get the values of the classical theta constants in
  // level 4 from the theta null point of level 4. This is done thanks
  // to the following functions
  theta_analytic:=GetAnalyticThetaNullPoint(thetas[1]);
  coord:=theta_analytic`coord; DDg:=Universe(coord);
  /* 
   * The value with characteristic [eps,eps'] with 2*eps=(e1,...eg) \in
   * {0,1}^g and 2*eps'=(e1',...eg') \in {0,1}^g  is given by
   * coord[e1*DDg.1+...eg*DDg.g+e1'*DDg.(g+1)+... eg'*DDg.(2g)]; For instance
   * the one for eps:=(1/2,0), eps'=(0,1/2) is
   */
  coord[DDg.1+DDg.4];


//////////////// second example in genus 2 //////////////////////
g:=2; q:=125; disc:=-2^4; E,kernels:=GenerateKernels(disc,g : q:=q);
AllCurves(kernels: filter:=1+q+g*Floor(2*Sqrt(q))); // All at once but output only the curves with the number of rational points indicated in the filter
AllCurves(kernels: filter:=true); // Same as above except we compute the required number of point automatically from the card of E
AllCurves(kernels: filter:=false); // If we want to find all twists
g:=2; q:=125; Defect0Curves(q,g); //shortcut

//////////////// third example in genus 2 ///////////////////////
g:=2; q:=271; disc:=-15*4; E,kernels:=GenerateKernels(disc,g : q:=q); // there are odd ell (=5) only for the two first lattices.  
AllCurves(kernels: filter:=true); 
g:=2; q:=271; Defect0Curves(q,g); //shortcut


//////////////// Quick example in genus 3 ///////////////////////////
EGlob:=EllipticCurve([0,0,1,-2*19,(19^2-1) div 4]); //CM curve over Q with disc=-19
FF:=GF(17); E:=WeierstrassModel(ChangeRing(EGlob,FF)); // End(E)=Z[pi]=max order
kernels:=EllipticCurveToKernels(E,3); // There is only one indecomposable principally polarized abelian threefold B in the isogeny class. 
time thetas:=IsogeniesFromKernels(kernels); // thetas computes a modular lift of the theta null point on B
// time thetas:=IsogeniesFromKernels(kernels: init:="none"); // try other init methods
/* The following function gather some information on a given B:
- obs is Serre's obstruction: obs is false if there is no obstruction
- chi18 is the value of chi18 at the modular lift
- curves is the list of all twists of the curve C/Fq such that B=Jac C. */
obs, chi18 := Genus3Obstruction(thetas[1], kernels[1]);
curves := AllTwists(thetas[1]);
// this last function computes the number of points and factorize the Weil
// polynomial of each curve in the previous list to check the result.
InfosOnCurves(curves);
allcurves:=AllCurves(kernels: filter:=false); // All at once
allcurves:=AllCurves(kernels: filter:=true); // All at once

///////////////// The examples below take much more time ///////////////////
///////////////// second example in genus 3 //////////////////////////
g:=3; q:=10313; disc:=DiscriminantDefect0EllipticCurve(q); E,kernels:=GenerateKernels(disc,g : q:=10313);
AllCurves(kernels: filter:=1+q+g*Floor(2*Sqrt(q)));
Defect0Curves(q,g); //Shortcut

g:=3; q:=131; Defect0Curves(q,g);
g:=3;q:=97; Defect0Curves(q,g);

///////////////// example in genus 4 //////////////////////////
g:=4; q:=59; disc:=DiscriminantDefect0EllipticCurve(q); //disc=-11
E,kernels:=GenerateKernels(disc,g : q:=q);
time thetas:=IsogeniesFromKernels(kernels);
[Genus4Igusa(thetas[i], kernels[i]): i in [1..#kernels]];
