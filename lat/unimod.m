//AttachSpec("lat.spec");
//MESSAGE FROM FABIEN: I had do modify slightly the code because it did not fit well with the presentation of ring as quotients of Z[T]. My modifications are marked with a "NFmodif:" when I skip a part of Markus code and add mine.

// Let P be a prime ideal over p.
// Returns a unit in o_p which is not a local norm from O_P.
function NonLocalNorm(P)
  R:= Order(P);
  E:= NumberField(R);
  S:= BaseRing(R);
  p:= Type(S) eq RngInt select Minimum(P) else P meet S;
  if IsInert(P) then
    u:= PrimitiveElement(p);
    assert not IsLocalNorm(E, u, p);
    elif IsRamified(P) then
      if Minimum(p) ne 2 then
        k,h:= ResidueClassField(p);
      else
        k,h:= quo< S | p^(2 * Valuation(S!2, p) +1) >;
      end if;
      repeat
        r:= Random(k);
        u:= r @@ h;
      until IsUnit(r) and not IsLocalNorm(E, u, p);
  else
    error "The prime ideal must not be split";
  end if;

  return S ! u;
end function;

function SpecialRamifiedUnit(P)
  assert IsRamified(P) and Minimum(P) eq 2;
  S:= Order(P);
  R:= BaseRing(S);
  E:= NumberField(S);
  p:= Type(R) eq RngInt select Minimum(P) else P meet R;
  e:= Valuation(Different(S), P);
  k, h:= quo< R | p^(1+2*Valuation(R ! 2, p)) >;
  repeat
    r:= Random(k);
    u:= r @@ h;
  until IsUnit(r) and not IsLocalNorm(E, u, p) and QuadraticDefect(u, p) eq e-1;
  u:= Type(u) eq RngIntElt select u mod 8 else NiceUnitSquareClassRepresentative(u, p);
  u0:= u-1;
  assert Valuation(u0, p) eq e-1;
  return u0;
end function;

function EvenLocalModularReps(P, m, val)
  S:= Order(P);
  FF:= FieldOfFractions(S);
  assert Degree(S) eq 2 and IsRamified(P) and Minimum(P) eq 2;
  pi:= PrimitiveElement(P);
  p:= Norm(pi);
  qi:= p/pi;
  v2:= val div 2;

  H:= Matrix(FF, 2, IsEven(val) select [0, p^v2, p^v2, 0] else [0, pi^val, qi^val, 0] );
  X:= m le 2 select MatrixRing(FF, 0) ! 0 else DiagonalJoin([ H^^((m-1) div 2) ]);

  if IsOdd(m) then
    u:= NonLocalNorm(P);
    assert IsEven(val);
    return [ HermitianLattice(DiagonalJoin( Matrix(FF, 1, [x*p^v2]), X )) : x in [1,u] ];
  end if;

  // the hyperbolic case
  L:= [ DiagonalJoin(H, X) ];
  e:= Valuation(Different(S), P);
  Bound1:= ((val+e) div 2) - 1;
  for k in [Ceiling(val/2)..Bound1] do
    Append(~L, DiagonalJoin( Matrix(FF, 2, [p^k, pi^val, qi^val, 0]), X) );
  end for;
  // the non-hyperbolic case
  u0:= SpecialRamifiedUnit(P);
  Bound2:= (val+e-1) div 2;
  for k in [Ceiling(val/2)..Bound2] do
     Append(~L, DiagonalJoin( Matrix(FF, 2, [p^k, pi^val, qi^val, -p^(val-k)*u0]), X) );
  end for;

  return [ HermitianLattice(f): f in L ];
end function;

function AllUnimod(E, m)
  assert Degree(E) eq 2;
  assert IsTotallyComplex(E);
  K:= BaseField(E);

  R:= Integers(E);
  d:= Discriminant(R);
  P:= [ f[1]: f in Factorization(d) ];

  PEven:= [ p: p in P | Minimum(p) eq 2 ];
  POdd := [ p: p in P | Minimum(p) ne 2 ];
  LocalEven:= [ EvenLocalModularReps(Decomposition(R, p)[1,1], m, 0) : p in PEven ];

  Forms:= [ HermitianFormWithInvariants(E, m, I, [0^^Degree(K)]) : I in Subsets(Set(P)) | IsEven(#I) ];

  Genera:= [];
  for F in Forms do
    d:= Determinant(F);
    M:= MaximalIntegralHermitianLattice(F);
    for p in POdd do
      u:= IsLocalNorm(E, d, p) select 1 else NonLocalNorm( Decomposition(R, p)[1,1] );
      L:= HermitianLattice( DiagonalMatrix( [(E ! 1)^^(m-1)] cat [E ! u] ) );
      M:= FindLattice(M, L, p);
    end for;
    if #PEven eq 0 then
      Append(~Genera, M);
      assert IsUnimodular(M);
      continue;
    end if;
    Cand:= [];
    for i in [1..#PEven] do
      X:= [ f : f in LocalEven[i] | IsRationallyEquivalent(M, f, PEven[i]) ];
      assert #X ne 0;
      Append(~Cand, [ FindLattice(M, x, PEven[i]) : x in X ]);
    end for;
    C:= CartesianProduct( Cand );
    for c in C do
      L:= &meet [ x : x in c ];
      assert IsUnimodular(L);
      Append(~Genera, L);
    end for;
  end for;

  return Genera;
end function;

function MyStab(M, G, size)
  S:= sub< G | 1 >;
  if size eq 1 then return S; end if;
  Orbit:= {@ HermiteForm(M) @};
  Path:= [ G.0 ];
  i:= 1;
  while (i le #Orbit) and (#S lt size) do
    for j in [1..Ngens(G)] do
      X:= HermiteForm(Orbit[i] * G.j);
      pos:= Index(Orbit, X);
      if pos eq 0 then
        Include(~Orbit, X);
        Append(~Path, Path[i] * G.j);
      else
        S:= sub< G | S, Path[i]*G.j*Path[pos]^-1 >;
      end if;
    end for;
    i := i+1;
  end while;
  assert #S eq size;
  M:= GL(Ncols(M), Rationals()) ! M;
  T:= ChangeRing(ChangeRing(S, Rationals()) ^ (M^-1), Integers());
  return T;
end function;

function MyGram(B, F)
  K:=BaseRing(F);
  if Category(K) eq FldQuad then
    C:= Matrix(Ncols(B), [ Conjugate(x): x in Eltseq(B) ]);
  else
    cinv:=Automorphisms(K)[2];
    C:= Matrix(Ncols(B), [ cinv(x): x in Eltseq(B) ]);
  end if;
  return B * Matrix(F) * Transpose(C);
end function;

function ZForms(B, F);
G:= MyGram(B, F);
K:= BaseRing(F);
return [ Matrix( Integers(), Nrows(B), [ Trace(x): x in Eltseq(a*G) ] ) : a in [1, K.1] ];
end function;

function SolveNorm(B, F, n)
  // We solve xGx^t = 1 where
  G:= n^-1 * MyGram(B, F);
  X:= LatticeWithGram( Matrix(Ncols(G), [ AbsoluteTrace(x) : x in Eltseq(G) ]) );

  S:= Matrix(BaseRing(G), ShortVectorsMatrix(X, 2, 2));
  SG:= S * G;
  S:= RowSubmatrix(S, [ i: i in [1..Nrows(S)] | IsOne(InnerProduct(SG[i], S[i])) ]);

  return S*B;
end function;

function AllUnimodOrder(O, m : Indecomposable:= false)
  assert IsAbsoluteOrder(O) and Degree(O) eq 2;
  K:= NumberField(O);
  ZK:= Integers(K);
  assert IsTotallyComplex(K);
  // a*c generates the order of conductor c
  a:= K ! ZK.2;
  _, w:= IsSquare(K ! Discriminant(ZK));
  A:= &cat [ GenusRepresentatives(x) : x in AllUnimod(K, m) ];
  All:= [];
  for L in A do
    B:= ChangeRing(Matrix([ Vector(x): x in AbsoluteBasis(L) ]), K);
    F:= Matrix(K, InnerProductMatrix(L));
    G:= MyGram(B, F);
    Aut:= AutomorphismGroup( [ Matrix(Integers(), 2*m, [ Trace(x): x in Eltseq(a*G) ]) : a in [1, K.1] ] );
    Append(~All, < B, F, Aut >);
  end for;

  C:=Factorization( Index(ZK, O) );
  C:= &cat[ [c[1]^^c[2]] : c in Reverse(C) ];
  d:= 1;

  for p in C do
    f:= d*p;
    O1:= sub< ZK | [ d*b: b in Basis(ZK) ] >;
    O2:= sub< ZK | [ f*b: b in Basis(ZK) ] >;

    AA:= [];
    for M in All do
      B, F, Aut:= Explode(M);
      MB:= Matrix(2*m, &cat[ Eltseq(x): x in Eltseq(B) ]);
      DM:= Matrix(Integers(), d * Solution(MB, Matrix(2*m, &cat[ Eltseq(x): x in Eltseq(a*B) ])));
      AutFp:= ChangeRing(Aut, GF(p));
      Subs:= MyOrbitsOfSpaces(AutFp, m);
      G:= MyGram(B, F);
      G:= Matrix( GF(p), 2*m, [ Eltseq(O1 ! x)[2] : x in Eltseq(G) ]);

      for s in Subs do
        Bs:= BasisMatrix(s[2]);
        // test if subspace is isotropic
            if not IsZero( Bs * G * Transpose(Bs) ) then continue; end if;
        // test if O1*L = M
            if Rank( VerticalJoin(Bs, Bs*ChangeRing(DM, GF(p)))) ne 2*m then continue; end if;
            // get Z-basis of L
                Bs:= VerticalJoin(ChangeRing(Bs,Integers()), MatrixRing(Integers(), 2*m) ! p);
                Bs:= Submatrix(LLL(Bs), 1,1,2*m,2*m);
                BB:= Matrix(K, Bs) * B;
                //
                //  if Indecomposable and #OrthogonalDecomposition(ZForms(BB, F)) ne 1 then continue; end if;
                // compute automorphism group of L
                    AutBB:= MyStab(Bs, Aut, #Aut div s[1]);

                    // The following test can all be commented out.
                        // action of O2 must fix the lattice L
                        MM:= Matrix(2*m, &cat[ Eltseq(x): x in Eltseq(BB) ]);
                    XX:= Matrix(2*m, &cat[ Eltseq(f*a*x): x in Eltseq(BB) ]);
                    tmp:= Solution(MM, XX);
                    assert CanChangeRing(tmp, Integers());

                    // action of O1 must map the lattice L to M;.
                        YY:= Submatrix(LLL(VerticalJoin(MM, 1/p*XX)), 1,1,2*m, 2*m);
                    tmp:= Solution(MB,YY);
                    assert CanChangeRing(tmp, Integers()) and Abs(Determinant(tmp)) eq 1;

                    // test if L is integral
                        assert CanChangeRing(MyGram(BB, F), O2);

                    // test if L eq Dual(L)
                        FF:= ZForms(BB, F);
                    tmp:= Matrix(K, FF[1])^-1 * BB;
                    DD:= Matrix(2*m, &cat[ &cat[ Eltseq(x): x in Eltseq((f*w*b)*tmp) ] : b in [ 1, f*a] ]);
                    tmp:= Solution(MM, DD);
                    assert CanChangeRing(tmp, Integers());
                    // end of tests

                        Append(~AA, < BB, F, AutBB>);
      end for;
    end for;

    All:= AA;
    d:= f;
  end for;

  if Indecomposable then
    All:= [ Append(x, 1) : x in All | #OrthogonalDecomposition(ZForms(x[1], x[2])) eq 1 ];
  else
    All:= [ Append(x, #OrthogonalDecomposition(ZForms(x[1], x[2]))) : x in All ];
  end if;
  return All;
end function;


function LocalLats(E, p, m, l)
  S:= Integers(E);
  P:= Decomposition(S, p)[1,1];
  if IsUnramified(p,S) then
    L0:= [* DiagonalMatrix(E, [ 1^^k ]) : k in [0..m] *];
    X:= [* Matrix(E, 0, []) *];
    for i in [1..l] do 
      X:= [* DiagonalJoin(x, p^i*y) : x in X, y in L0 | Ncols(y) + Ncols(x) le m *]; 
    end for;
    X:= [ DiagonalJoin(y,x): y in L0, x in X | Ncols(y) + Ncols(x) eq m ];
  else
    l:= 2*l;
    pi:= E ! PrimitiveElement(P);
    u:= NonLocalNorm(P);
    H0:= Matrix(E, 2, [0,1,1,0]);
    H1:= Matrix(E, 2, [0,pi,Trace(pi)-pi,0]);
    if IsOdd(Minimum(p)) then
      L0:= [* DiagonalMatrix(E, [1^^k] cat [x] ) : x in [1,u], k in [0..m-1] *];
      L1:= [* DiagonalJoin( [H1^^k] ): k in [1..m div 2] *];
    else
      Start:= EvenLocalModularReps(P, 1, 0) cat EvenLocalModularReps(P, 2, 0);
      L0:= [* DiagonalJoin( <Matrix(E, x`Form)> cat <H0 : i in [1..k]> ) : k in [0..(m-Rank(x)) div 2], x in Start *];
      Start:= EvenLocalModularReps(P, 2, 1);
      L1:= [* DiagonalJoin( <Matrix(E, x`Form)> cat <H1 : i in [1..k]> ) : k in [0..(m div 2)-1], x in Start *];
    end if;
    X:= [* Matrix(E, 0, []) *];
    for i in [1..l] do
      X:= X cat [* DiagonalJoin(x, p^(i div 2)*y) : x in X, y in (IsEven(i) select L0 else L1) | Ncols(y) + Ncols(x) le m *];
    end for;
    X:= [ x: x in X | Ncols(x) eq m] cat [ DiagonalJoin(y, x) : x in X, y in L0 | Ncols(x) + Ncols(y) eq m ];
  end if;

  X:= [ HermitianLattice(x) : x in X ];

  if Minimum(p) eq 2 and IsRamified(p,S) then
    Y:= [];
    for x in X do
      if forall{y: y in Y | not IsLocallyIsometric(x,y,p) } then Append(~Y, x); end if;
    end for;
    X:= Y;
  else
    assert forall{ <i,j>: i in [1..j-1], j in [1..#X] | not IsLocallyIsometric(X[i], X[j], p) };
  end if;
  return X;
end function;


function AllUnimodOrder2(O, m : Indecomposable:= false)
  assert IsAbsoluteOrder(O) and Degree(O) eq 2;
  K:= NumberField(O);
  assert IsTotallyComplex(K) and AbsoluteDegree(K) eq 2;
  ZK:= Integers(K);

  dK:= Abs(Discriminant(ZK));
  f:= Index(ZK, O);

  a:= K ! ZK.2;
  _, w:= IsSquare(K ! -dK);

  P:= [ p: p in PrimeDivisors(f*dK) | not IsSplit(p, ZK) ];
  Forms:= [ HermitianFormWithInvariants(K, m, I, [0]) : I in Subsets(Set(P)) | IsEven(#I) ];
  Local:= [ LocalLats(K, p, m, Valuation(f, p)) : p in P ];
  Ms:= [];
  for F in Forms do
    M:= MaximalIntegralHermitianLattice(F);
    X:= [ [ FindLattice(M, x, P[i]) : x in Local[i] | IsRationallyEquivalent(M, x, P[i]) ] : i in [1..#P] ];
    Ms cat:= [ &meet( [ x: x in c ] ) : c in CartesianProduct(X) ];
  end for;

  A:= &cat[ GenusRepresentatives(M): M in Ms ];
  TODO:= [];
  All:= [];
  for L in A do
    B:= ChangeRing(Matrix([ Vector(x): x in AbsoluteBasis(L) ]), K);
    F:= Matrix(K, InnerProductMatrix(L));
    G:= MyGram(B, F);
    Aut:= AutomorphismGroup( [ Matrix(Integers(), 2*m, [ Trace(x): x in Eltseq(a*G) ]) : a in [1, K.1] ] );
    Append(~TODO, < B, F, Aut >);
    if f eq 1 and IsUnimodular(L) then
      OO:= OrthogonalDecomposition(L);
      if (not Indecomposable or #OO eq 1) then Append(~All, < B, F, Aut, #OO > ); end if;
    end if;
  end for;

  C:= &cat[ [c[1]^^c[2]] : c in Reverse(Factorization(f)) ];
  d:= 1;
  for p in C do
    f:= d*p;
    O1:= sub< ZK | [ d*b: b in Basis(ZK) ] >;
    O2:= sub< ZK | [ f*b: b in Basis(ZK) ] >;

    TODO2:= [];
    for M in TODO do
      B, F, Aut:= Explode(M);
      G:= MyGram(B, F);
      G:= Matrix( GF(p), 2*m, [ Eltseq(O1 ! x)[2] : x in Eltseq(G) ]);
      MB:= Matrix(2*m, &cat[ Eltseq(x): x in Eltseq(B) ]);
      DM:= Matrix(Integers(), d * Solution(MB, Matrix(2*m, &cat[ Eltseq(x): x in Eltseq(a*B) ])));
      AutFp:= ChangeRing(Aut, GF(p));
      for k in [m..2*m] do
        Subs:= MyOrbitsOfSpaces(AutFp, k);
        for s in Subs do
          Bs:= BasisMatrix(s[2]);
          // test if subspace is isotropic
          if not IsZero( Bs * G * Transpose(Bs) ) then continue; end if;
          // test if O1*L = M
          if Rank( VerticalJoin(Bs, Bs*ChangeRing(DM, GF(p)))) ne 2*m then continue; end if;
          // get Z-basis of L
          Bs:= VerticalJoin(ChangeRing(Bs,Integers()), MatrixRing(Integers(), 2*m) ! p);
          Bs:= Submatrix(LLL(Bs), 1,1,2*m,2*m);
          BB:= Matrix(K, Bs) * B;

          // the next 3 tests should always work:
          // test if L is integral
          assert CanChangeRing(MyGram(BB, F), O2);
          // action of O2 must fix the lattice L
          MM:= Matrix(2*m, &cat[ Eltseq(x): x in Eltseq(BB) ]);
          XX:= Matrix(2*m, &cat[ Eltseq(f*a*x): x in Eltseq(BB) ]);
          tmp:= Solution(MM, XX);
          assert CanChangeRing(tmp, Integers());
          // action of O1 must map the lattice L to M;.
          YY:= Submatrix(LLL(VerticalJoin(MM, 1/p*XX)), 1,1,2*m,2*m);
          tmp:= Solution(MB,YY);
          assert CanChangeRing(tmp, Integers()) and Abs(Determinant(tmp)) eq 1;

          // compute automorphism group of L
          AutBB:= MyStab(Bs, Aut, #Aut div s[1]);

          // test if L eq Dual(L)
          FF:= ZForms(BB, F);
          tmp:= Matrix(K, FF[1])^-1 * BB;
          DD:= Matrix(2*m, &cat[ &cat[ Eltseq(x): x in Eltseq((f*w*b)*tmp) ] : b in [ 1, f*a] ]);
          tmp:= Solution(MM, DD);
          if CanChangeRing(tmp, Integers()) then
	    OO:= OrthogonalDecomposition(ZForms(BB, F));
	    if (not Indecomposable or #OO eq 1) then Append(~All, < BB, F, AutBB, #OO > ); end if;
          end if;
          Append(~TODO2, < BB, F, AutBB>);
        end for;
      end for;
    end for;

    TODO:= TODO2;
    d:= f;
  end for;

  return All;
end function;


function TwoGen(I)
  O:= Order(I);
  a:= Minimum(I);
  B:= Basis(I);
  Oa:= O*a;
  repeat
    repeat x:= &+[ b * Random(-100,100) : b in B ]; until x ne 0;
    Ox:= O*x;
    b:= Integers() ! (Minimum(Ox)/a);
  until	GCD(a, b) eq 1 and I eq Oa+Ox;
  return a, x, b;
end function;

// returns a1,a2 in I and b1,b2 in I^-1 s.t. a1b1+a2b2 = 1
function LinKomb(I)
  d:= Denominator(I);
  a,x,b:= TwoGen(d*I);
  g,r,s:= XGCD(Integers() ! a, Integers() ! b); assert g eq 1;
  return <r*a/d, x/d>, <d, d*s*b/x>;
end function;

function PB(M)
  if ISA(Type(M), Mtrx) then M:= Rows(M); end if;
  V:= Universe(M);
  f:= map< V -> VectorSpace(Rationals(), 2 * Degree(V)) | v :-> &cat[Eltseq(x) : x in Eltseq(v)],  w :-> Partition(Eltseq(w), 2) >;
  K:= BaseRing(V);
  O:= Integers(K);
  w:= K ! Basis(O)[2];

  T1:= Matrix(M @ f);

  B:= [* *];
  while true do
    // Get a Z-basis of the module.
    MM:= M @ f;
    MM:= [ x: x in LLL(MM) | not IsZero(x) ];
    if #MM eq 0 then break; end if;
    M:= MM @@ f;

    // Find the largest order over which M is a module.
    W:= Solution( Matrix(MM), Matrix([ f(w*m) : m in M ]) );
    W, c:= IntegralMatrix(W);
    R:= sub< O | [1, c*w] >;

    // Find a linear functional l in M^* such that a*l notin M^* for all a in O \ R.
    _,_,T:= SmithForm(W);
//    II:= Transpose( Solution( Transpose(Matrix(MM)), MatrixRing(Rationals(), #MM) ! 1) );
//    l:= II * Matrix(Rationals(), 1, [ T[i,1]: i in [1..#M] ] );

    // Find a splitting V = Kv + N, where l(v) = 1 and l(n) = l(w*n) for all n in N.
//    X:= Matrix( [ [ (f(a*b)*l)[1] : a in [1,w] ]: b in M ]);
    X:= Transpose(Matrix([ Transpose(T)[1] , 1/c * ChangeRing(Transpose(W*T)[1], Rationals()) ]));
    v, N:= Solution( X, Matrix(Rationals(), 2, [1, 0]) );
    v:= &+[ v[1,i] * M[i] : i in [1..#M] ];
    N:= BasisMatrix(ChangeRing(N, K)) * Matrix(M);
    T:= VerticalJoin( Matrix([v]), N );

    // Project the module on Kv.
    M1:= [ S[i,1] : i in [1..Nrows(S)] ] where S:= Solution(T, Matrix(M));
    A:= ideal< R | M1 >;
    assert R eq MultiplicatorRing(A);
    FF:= FieldOfFractions(R);

    // The Pohst-Bosma trick to get the splitting of M in the form A*x + (M meet N).
    a,b:= LinKomb(A);
    h:= [];
    MM, d1 := IntegralMatrix(Matrix([ Eltseq(FF!x) : x in M1 ]));
    for x in a do
      s:= Solution(MM, Vector(Integers(), Eltseq(FF ! (d1 * x))));
      Append(~h, &+[s[i] * M[i] : i in [1..#M]]);
    end for;
    x:= b[1]*h[1] + b[2]*h[2];
    M:= [ M[i] - M1[i]*x : i in [1..#M] ];
    Append(~B, < A, x >);
  end while;

  // Keep your fingers crossed ...
  T2:= Matrix([ f(a*b[2]) : a in Basis(b[1]), b in B ] );
  S,d:= IntegralMatrix(Solution(T2, T1));
  assert d eq 1 and Set(ElementaryDivisors(S)) eq {1};

  return B;
end function;

function IsProj(M, R, p)
  if ISA(Type(M), Mtrx) then M:= Rows(M); end if;
  m:= Degree(M[1]);
  K:= NumberField(R);
  ZK:= Integers(K);
  v:= Valuation(Index(ZK, R), p);
  f:= map< VectorSpace(K, m) -> VectorSpace(Rationals(), 2*m) | v :-> &cat[Eltseq(x) : x in Eltseq(v)],  w :-> Partition(Eltseq(w), 2) >;
  M1:= Matrix( M @ f );
  MO:= VerticalJoin(M1, Matrix( [ f(a*m) : m in M ])) where a:= Basis(ZK)[2];
  MO:= Submatrix(LLL(MO), 1,1,2*m,2*m);
  return Valuation(Determinant(M1)/Determinant(MO), p) eq v*m;
end function;

function IsNorm(x,d)
  x:= Numerator(x) * Denominator(x);
  P:= Set(PrimeDivisors(x)) join Set(PrimeDivisors(2*d));
  return forall{ p: p in P | HilbertSymbol(x, d, p) eq 1 };
end function;

red8:= func< x | (Denominator(y) * Numerator(y)) mod 8 where y:= Rationals() ! x >;

function local_basis(B, p)
  X:= [];
  for b in B do
    v:= Valuation(Norm(b[1]), 2);
    Ba:= Basis(b[1]);
    repeat x:= Ba[1] * Random(-20,20) + Ba[2] * Random(-20,20); until Valuation( Norm(x), 2 ) eq v;
    Append(~X, x*b[2]);
  end for;
  return X;
end function;

function ExistsFamily(R, A, a)
  M:= A[1];
  F:= A[2];
  K:= NumberField(R);
  ZK:= Integers(K);
  dK:= Discriminant(ZK);
  aut:= Automorphisms(K)[2];
  g:= Nrows(F);

  // Test if Det(F) == 1
  if IsEven(g) and not IsNorm(Rationals() ! Determinant(F), dK) then return false, _; end if;

  // Test if M_p is locally free
  f:= Index(ZK, R);
  B:= PB(M);
  for p in PrimeDivisors(a) do
    v:= Valuation(f, p);
    if v ne 0 and exists{b: b in B | Valuation(Index(ZK, MultiplicatorRing(b[1])), p) ne v} then return false, _; end if;
  end for;

  if IsEven(g) then
    for p in PrimeDivisors( GCD(f, a) ) do
      if p eq 2 then continue; end if;
      X:= local_basis(B, p);
      X:= Matrix(X);
      G:= X * F * Transpose(Matrix(Ncols(X), [ aut(x): x in Eltseq(X) ]));
      d:= GF(p) ! Rationals() ! Determinant(G);
      assert d ne 0;
      if not IsSquare(d) then return false, _; end if;
    end for;
  end if;

  // If a is odd, we are done.
  if IsOdd(a) then return true, 0; end if;

  // Otherwise compute \ell_2:
  if IsOdd(dK) and f mod 4 ne 0 then N:= {1,3,5,7};
  elif dK mod 8 eq 0 and IsOdd(f) then N:= {1, (1-dK div 4) mod 8};
  elif (f^2*dK) mod 32 eq 0 then N:= {1};
  else N:= {1,5};
  end if;

  X:= local_basis(B, 2);
  X:= Matrix(X);
  G:= X * F * Transpose(Matrix(Ncols(X), [ aut(x): x in Eltseq(X) ]));
  u:= red8(Determinant(G));
  assert IsOdd(u);

  if IsEven(g) and IsEven(f*dK) and forall{ i: i in [1..g] | Valuation(Rationals() ! G[i,i], 2) gt 0 } then
    return false, _;		// lattice even
  elif IsEven(g) and u notin N then
    return false, _;		// det cond. missed
  elif IsOdd(f) or 3 in N or 7 in N then
    return true, IsOdd(g) select u else 1;		// now 1,...,1 or u,...,u works
  end if;

  FF:= FieldOfFractions(R);
  i:= 1;
  while i le g do
    M:= MatrixRing(K, g) ! 1;
    if exists(j){j: j in [i..g] | Valuation(Rationals() ! G[j,j], 2) eq 0} then
      for k in [i..g] do
        if k ne j then
	  M[k] -:= G[k,j]/G[j,j] * M[j];
	end if;
      end for;
      SwapRows(~M,i,j);
      G:= M * G * Transpose(Matrix(Ncols(X), [ aut(x): x in Eltseq(M) ]));
      G[i,i]:= red8(G[i,i]);
      i +:= 1;
    else
      ok:= exists(j){j: j in [i+1..g] | Valuation(Norm(G[j,i]), 2) eq 0 and IsOdd(Denominator(FF ! G[j,i])) };
      assert ok;
      assert Valuation(Rationals() ! G[1,1], 2) eq 0;

      d:= G[i,i]*G[j,j]-Norm(G[i,j]);
      for k in [i+1..g] do
        if k ne j then
	  M[k] -:= ((G[j,j] *G[k,i] - G[j,i]*G[k,j]) * M[i] + (G[i,i]*G[k,j] - G[i,j]*G[k,i]) * M[j]) / d;
	end if;
      end for;
      SwapRows(~M, i+1,j);
      j:= i+1;
      G:= M * G * Transpose(Matrix(Ncols(X), [ aut(x): x in Eltseq(M) ]));

      if Valuation(Rationals() ! G[j,j], 2) gt 0 then
        AddRow(~G, 1, 1, i);
        AddColumn(~G, 1, 1, i);
	M:= Parent(M) ! 1;
	d:= G[i,i]*G[j,j]-Norm(G[i,j]);
	M[1] -:= ((G[j,j] *G[1,i] - G[j,i]*G[1,j]) * M[i] + (G[i,i]*G[1,j] - G[i,j]*G[1,i]) * M[j]) / d;
        G:= M * G * Transpose(Matrix(Ncols(X), [ aut(x): x in Eltseq(M) ]));
	assert forall{j: j in [2..g] | G[1,j] eq 0};
	G[1,1]:= red8(G[1,1]);
      end if;
      // restart loop
    end if;
  end while;

  ok, D:= IsDiagonal(G);
  assert ok;
  ChangeUniverse(~D, Integers());

  c:= &*[ Integers() | HilbertSymbol(D[i], D[j], 2) : j in[i+1..g], i in [1..g] ];
  for l in [1..7 by 2] do
    if IsOdd(g) and red8(l * u) notin N then continue; end if;
    if HilbertSymbol(l,l,2)^((g*(g-1)) div 2) eq c then return true, l; end if;
  end for;

  return false, _;
end function;
