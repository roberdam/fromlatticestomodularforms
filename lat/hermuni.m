//AttachSpec("lat.spec");

// Let P be a prime ideal over p.
// Returns a unit in o_p which is not a local norm from O_P.
function NonLocalNorm(P)
  R:= Order(P);
  E:= NumberField(R);
  S:= BaseRing(R);
  p:= Type(S) eq RngInt select Minimum(P) else P meet S;
  if IsInert(P) then
    u:= PrimitiveElement(p);
    assert not IsLocalNorm(E, u, p);
  elif IsRamified(P) then
    if Minimum(p) ne 2 then
      k,h:= ResidueClassField(p);
    else
      k,h:= quo< S | p^(2 * Valuation(S!2, p) +1) >;
    end if;
    repeat
      r:= Random(k);
      u:= r @@ h;
    until IsUnit(r) and not IsLocalNorm(E, u, p);
  else
    error "The prime ideal must not be split";
  end if;

  return S ! u;
end function;

function SpecialRamifiedUnit(P)
  assert IsRamified(P) and Minimum(P) eq 2;
  S:= Order(P);
  R:= BaseRing(S);
  E:= NumberField(S);
  p:= Type(R) eq RngInt select Minimum(P) else P meet R;
  e:= Valuation(Different(S), P);
  k, h:= quo< R | p^(1+2*Valuation(R ! 2, p)) >;
  repeat
    r:= Random(k);
    u:= r @@ h;
  until IsUnit(r) and not IsLocalNorm(E, u, p) and QuadraticDefect(u, p) eq e-1;
  u:= Type(u) eq RngIntElt select u mod 8 else NiceUnitSquareClassRepresentative(u, p);
  u0:= u-1;
  assert Valuation(u0, p) eq e-1;
  return u0;
end function;

function EvenUniModularReps(P, m)
  assert IsRamified(P);
  S:= Order(P);
  FF:= FieldOfFractions(S);
  pi:= PrimitiveElement(P);
  p:= Norm(pi);

  H:= Matrix(FF, 2, [0, 1, 1, 0]);
  X:= m le 2 select MatrixRing(FF, 0) ! 0 else DiagonalJoin([ H^^((m-1) div 2) ]);

  if IsOdd(m) or Minimum(P) ne 2 then
    u:= NonLocalNorm(P);
    return [ HermitianLattice(DiagonalJoin( Matrix(FF, 1, [x]), X) ) : x in [1,u] ];
  end if;

  // the hyperbolic case
  L:= [ DiagonalJoin(H, X) ];
  e:= Valuation(Different(S), P);
  Bound1:= (e div 2) - 1;
  for k in [0..Bound1] do
    Append(~L, DiagonalJoin( Matrix(FF, 2, [p^k, 1, 1, 0]), X) );
  end for;
  // the non-hyperbolic case
  u0:= SpecialRamifiedUnit(P);
  Bound2:= (e-1) div 2;
  for k in [0..Bound2] do
     Append(~L, DiagonalJoin( Matrix(FF, 2, [p^k, 1, 1, -p^(-k)*u0]), X) );
  end for;
  Bound1:= ((e-1) div 2);

  return [ HermitianLattice(f): f in L ];
end function;

function AllUnimod(E, m)
  assert Degree(E) eq 2;
  assert IsTotallyComplex(E);
  K:= BaseField(E);

  R:= Integers(E);
  d:= Discriminant(R);
  P:= [ f[1]: f in Factorization(d) ];
  // n:= [ 0 : p in RealPlaces(K) | #Decomposition(E, p) eq 1 ];

  PEven:= [ p: p in P | Minimum(p) eq 2 ];
  POdd := [ p: p in P | Minimum(p) ne 2 ];
  LocalEven:= [ EvenUniModularReps(Decomposition(R, p)[1,1], m) : p in PEven ];

  Forms:= [ HermitianFormWithInvariants(E, m, I, [0^^Degree(K)]) : I in Subsets(Set(P)) | IsEven(#I) ];

  Genera:= [];
  for F in Forms do
    d:= Determinant(F);
    M:= MaximalIntegralHermitianLattice(F);
    for p in POdd do
      L:= HermitianLattice( DiagonalMatrix( [(E ! 1)^^(m-1)] cat [E ! d] ) );
      M:= FindLattice(M, L, p);
    end for;
    if #PEven eq 0 then 
      Append(~Genera, M);
      continue;
    end if;
    Cand:= [];
    for i in [1..#PEven] do
      X:= [ f : f in LocalEven[i] | IsRationallyEquivalent(M, f, PEven[i]) ];
      assert #X ne 0;
      Append(~Cand, [ FindLattice(M, x, PEven[i]) : x in X ]);
    end for;
    C:= CartesianProduct( Cand );
    for c in C do
      L:= &meet [ x : x in c ];
      Append(~Genera, L);
    end for;
  end for;

  return Genera;
end function;
