function NextPrimePower(n)
    if #Factorization(n) eq 1 then
        return n;
    else
        return NextPrimePower(n+1);
    end if;
end function;

/*Given the discriminant D of an imaginary quadratic order and a bound N
  the function returns the smallest  [q,m] (q=p^n with the option p<>2)
  with q < N such that the ring Z[T]/(T^2+mT+q) has Discriminant D*/

NonMaxOrd:=function(D,N : oddprime:=true)
//    Candidates:=[];
	if oddprime then q:=5; else q:=2; end if;
	while q lt N do
		b,s:=IsSquare(D+4*q);
		if b and Gcd(s,q) eq 1 then 
			return [q,-s]; end if;
		if oddprime then
			repeat
        		q:=NextPrimePower(q+1);
			until IsOdd(q);
		else
			q:=NextPrimePower(q+1);	
        end if;
	end while;
	return [];
end function;	

//Given a trace m and a prime power q return an elliptic curve with trace m over Fq and minimal endomorphism ring Z[pi].
EllipticCurveWithMininalEndomorphismRing:=function(m,q)
	k:=GF(q);
 	p:=Characteristic(k);
	if IsDivisibleBy(m,p) then 
		return "the elliptic curve is not ordinary";
	end if;
	Elist:=[];
    for a in k do
        Ej:=EllipticCurveWithjInvariant(a);
        for E in Twists(Ej) do
            if Trace(E) eq m then
                Append(~Elist,E);
            end if;
        end for;
    end for;
    error if #Elist eq 0, "No Elliptic curve over ", k," with trace ", m;
    if #Elist eq 1 then
        return Elist[1];
    else
        for E in Elist do
            D1,D2:=OrdinaryEndomorphismRingDiscriminant(E);
            if D1 eq D2 then
                return SimplifiedModel(E);
            end if;
        end for;
    end if;
end function;

