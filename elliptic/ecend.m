/* Functions to compute the endomorphism ring of an elliptic curve */

intrinsic ReducedModularPolynomial(ell::RngIntElt, p::RngIntElt) -> RngMPolElt
{ The modular polynomial of level ell over Fp. }
	Phi := ClassicalModularPolynomial(ell);  return ChangeRing(Parent(Phi),GF(p))!Phi;
end intrinsic;

function mroots(f) return &cat[[a[1]:i in [1..a[2]]]:a in r] where r:=Roots(f); end function;

function distance_to_floor(j,ell)
	F := Parent(j);;
	p := Characteristic(F);
	assert p gt 0;
	phi := ReducedModularPolynomial (ell,p);
	R<t> := PolynomialRing(F);
	r := mroots(Evaluate(phi,[t,j]));
	if #r lt ell+1 then return 0; end if;
	j0:= [j,j,j];
	j1 := [a:a in r][1..3];
	d := 1;
	// walk 3 non-backtracking paths in parallel, return as soon as one hits the floor (abort and return -1 if we prove curve is supersingular)
	B := Floor(Log(ell,4*#F)) div 2;
	while true do
		r := [mroots(ExactQuotient(Evaluate(phi,[t,j1[i]]),t-j0[i])):i in [1..3]];
		m := Min([#a:a in r]);
		if m eq 0 then return d; end if;
		assert m eq ell;
		j0 := j1;
		j1 := [a[1]:a in r];
		d +:= 1;
		if d gt B then return -1; end if;
	end while;
end function;

BIGELL := 32;

intrinsic OrdinaryEndomorphismRingDiscriminant(E::CrvEll) -> RngIntElt, RngIntElt
{ For an ordinary elliptic curve E/Fq, the discriminant of End(E), followed by the discriminant of the Z[pi_E]. }
	F := BaseRing(E);
	require IsFinite(F) and IsField(F): "E must be defined over a finite field.";
	p := Characteristic(BaseRing(E));
	ap := TraceOfFrobenius(E);
	require ap mod p ne 0: "The specified curve is supersingular.";
	D := ap^2 - 4*#F;
	D0 := FundamentalDiscriminant(D);
	if D0 eq D then return D0, D; end if;
	if jInvariant(E) eq F!0 then assert D0 eq -3; return D0, D; end if;
	if jInvariant(E) eq F!1728 then assert D0 eq -4; return D0, D; end if;
	v := Integers()!Sqrt(ExactQuotient(D,D0));
	L := Sort(PrimeDivisors(v));
	c := 1;
	j := jInvariant(E);
	for ell in L do
		if ell ge BIGELL then break; end if;
		d := distance_to_floor(j,ell);
		assert d ge 0;
		c *:= ell^(Valuation(v,ell)-d);
	end for;
	L := [ell:ell in L|ell gt BIGELL];
	require #L le 1: "You need to increase BIGELL to handle this case (and possibly download some modular polynomials) or switch to Bisson-Sutherland";
	if #L eq 1 then
		ell := L[1];
		n := Valuation(v,ell);
		e := 0;
		while e lt n do
			HD := c^2*ell^(2*e)*D0;
			if Abs(HD) gt 4*10^6 then printf "Computing H_D(X) for D = %o, this could take a while (Magma is not good at this, you might want to call out to GP)...", D; end if;
			if Evaluate(HilbertClassPolynomial(HD),j) eq 0 then break; end if;
			e +:= 1;
		end while;
		c *:= ell^e;
	end if;
	return c^2*D0, D;
end intrinsic;
