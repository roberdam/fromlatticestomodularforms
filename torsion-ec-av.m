// elliptic curve basis of l-torsion
function EllipticBasisTorsion(E,l)
  T:=TorsionSubgroupScheme(E,l);
  pts,F2:=PointsOverSplittingField(T);
  E2:=ChangeRing(E,F2);
  base:=false;
  while base ne true do
    b1:=Random(pts);
    b2:=Random(pts);
    w:=WeilPairing(b1,b2,l);
    if Order(w) eq l then 
      return [(E(F2))!b1,(E(F2))!b2];
    end if;
  end while;
end function; 

// write a n-torsion point pt in a basis b1,b2 of E[n]
// ord: order of the basis (compute it if not specified)
function DecompositionTorsionBasis(pt,B : ord:=0)
  b1:=B[1];b2:=B[2];
  Er:=Scheme(b1);
  if ord eq 0 then 
    order:=Order(b1);
  else 
    order:=ord;
  end if;
  Zn:=Integers(order);
  pt2:=pt;
  w:=WeilPairing(pt2,b1,order);
  alpha2:=0;
  while w ne 1 do
    pt2:=pt2-b2;
    alpha2:=alpha2+1;
    w:=WeilPairing(pt2,b1,order);
  end while;
  alpha1:=Log(b1,pt2);
  return [Zn!alpha1,Zn!alpha2];
end function;   
  
// the representation of the element f of R seen as an element of End(E[n]) otimes Q
// on a basis B of the n-torsion. 
 
function RepresentationOnEllipticTorsion(f,B : ord:=0, Mpi:=[])
  if Mpi cmpeq [] then
    b1:=B[1];b2:=B[2];
      FF:=BaseRing(Curve(b1));
        deg:=Degree(FF);
        Er:=Scheme(b1);
        if ord eq 0 then
        order:=Order(b1);
        else
        order:=ord;
        end if;
        pi:=FrobeniusMap(Er,deg);
        Mpi:=Matrix([DecompositionTorsionBasis(pi(b),B : ord:=order) : b in B]);
  end if;
  Zn:=BaseRing(Mpi);
    Id:=IdentityMatrix(Zn,2);
    fc:=Eltseq(f);
    return fc[1]*Id+fc[2]*Mpi;
end function;

// here f between  R^r and R^g is given by a r*g matrix.
// we want it representation on n-torsion on of the corresponding av
// we take each vector column and find its representation on the n-torsion
function RepresentationOnTorsion(f,B : ord:=0)
    r:=NumberOfRows(f);
    g:=NumberOfRows(Transpose(f));
  b1:=B[1];b2:=B[2];
  FF:=BaseRing(Curve(b1));
    deg:=Degree(FF);
    Er:=Scheme(b1);
    if ord eq 0 then
    order:=Order(b1);
    else
    order:=ord;
    end if;
    pi:=FrobeniusMap(Er,deg);
    Mpi:=Matrix([DecompositionTorsionBasis(pi(b),B : ord:=order) : b in B]);
  M:=BlockMatrix(r,g,[RepresentationOnEllipticTorsion(fe,B : ord:=ord, Mpi:=Mpi) : fe in Eltseq(f)]);
  return M;
end function;

// return the basis of a Z/nZ-module
function BasisZn(K)
  B:=Basis(K);
  g2:=Degree(K);
  r:=Rank(K);
  Zn:=BaseRing(K);
  n:=#Zn;
  G:=AbelianGroup([n : i in [1..g2]]);
  H:=sub<G | [&+[Integers()!(B[i][j])*G.j : j in [1..g2]] : i in [1..r]]>;
  basis:=[];
  for gg in Generators(H) do
    coef:=Eltseq(G!gg);
    Append(~basis,[Zn!co : co in coef]);
  end for;
  return #Generators(H) eq (g2 div 2),basis;
end function; 



// Given a basis of a kernel computed as the ker of the linear matrix map acting on B
// compute the real geometric points of the kernel
function AbstractKernelToKernel(K,B)
  b1:=B[1];b2:=B[2];
  g:=#(K[1]) div 2;
//  V:=Basis(K);
  ptlist:=[];
//  for v in V do 
//    ptlist:=ptlist cat [<Integers()!(v[2*i-1])*b1+Integers()!(v[2*i])*b2 :  i in [1..g]>];
//  end for;
  for v in K do
      Append(~ptlist,<Integers()!(v[2*i-1])*b1+Integers()!(v[2*i])*b2 :  i in [1..g]>);
  end for;
  return ptlist;
end function;
