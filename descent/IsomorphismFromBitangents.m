
  
///////// Useful functions  
  
  
//// convert a*X+b*Y*c*Z en [a :b :c] in a given projective plane

function Eq2Pt(f,PP)
	return PP![Coefficient(f,1,1),Coefficient(f,2,1),Coefficient(f,3,1)];
end function;


////// An elementary function to map 4 given ordered points of P^2 in a list S1
// to 4 others points in a list S2

function PlaneIso(S1,S2,PP)
	T1:=TranslationOfSimplex(PP,[s : s in S1]);
	T2:=TranslationOfSimplex(PP,[s : s in S2]);
return Inverse(T1)*T2;
end function;



// enumeration of the 315 syzygetic tetrads (Dolgachev p.117)
// from a th ;

function SyzygeticTetrads(D)
	Tetrads:={};
	beta:=[D.0 : i in [1..7]];
	beta[1]:=D.1+D.2+D.3+D.4+D.5+D.6;
	beta[2]:=D.3+D.5+D.6;
	beta[3]:=D.2+D.3+D.6;
	beta[4]:=D.1+D.3+D.4;
	beta[5]:=D.1+D.4+D.6;
	beta[6]:=D.1+D.2+D.5;
	beta[7]:=D.2+D.4+D.5;
	odd:=Seqset(beta);
	for i:=1 to 6 do
  	  for j:=i+1 to 7 do
    	  odd:=odd join {beta[i]+beta[j]};
  	  end for;
	end for;
	Tetrads:=[];
  	for b1 in odd do
  	  for b2 in odd diff {b1} do
    	for b3 in odd diff {b1,b2} do
      		if b1+b2+b3 in odd then 
		  	  Append(~Tetrads,{b1,b2,b3,b1+b2+b3}); 
  	  		end if;
    	end for;
  	 end for;
	end for;
	return Tetrads;
end function;




////// Function to get a bitangent with characteristic eps in the base D
/// from the Weber's moduli L

function Bitangent(eps,L,PP)
	P<x1,x2,x3>:=CoordinateRing(PP);
	alpha_1:=L[1];
alphaprime_1:=L[2];
alphabis_1:=L[3];
alpha_2:=L[4];
alphaprime_2:=L[5];
alphabis_2:=L[6];
alpha_3:=L[7];
alphaprime_3:=L[8];
alphabis_3:=L[9];
M:=-Matrix([[L[1],L[4],L[7]],[L[2],L[5],L[8]],[L[3],L[6],L[9]]]);
Mp:=Matrix([[1/L[1],1/L[4],1/L[7]],[1/L[2],1/L[5],1/L[8]],[1/L[3],1/L[6],1/L[9]]]);
N:=Mp^(-1)*M;
v1:=(N[1,1]*x1+N[1,2]*x2+N[1,3]*x3);
v2:=(N[2,1]*x1+N[2,2]*x2+N[2,3]*x3);
v3:=(N[3,1]*x1+N[3,2]*x2+N[3,3]*x3);
//print (v1+v2+v3)+x1+x2+x3;
D:=Parent(eps);
p:=D.0;
beta1:=D.1+D.2+D.3+D.4+D.5+D.6;
beta2:=D.3+D.5+D.6;
beta3:=D.2+D.3+D.6;
beta4:=D.1+D.3+D.4;
beta5:=D.1+D.4+D.6;
beta6:=D.1+D.2+D.5;
beta7:=D.2+D.4+D.5;
case eps:
 when beta1: return Eq2Pt(x1,PP);
 when  beta2: return Eq2Pt(x2,PP);
 when  beta3: return Eq2Pt(x3,PP);
 when p+beta2+beta3: return Eq2Pt(v1,PP);
 when p+beta3+beta1: return Eq2Pt(v2,PP);
 when  p+beta1+beta2: return Eq2Pt(v3,PP);
 when  beta4: return Eq2Pt(x1+x2+x3,PP);
 when p+beta1+beta4: return Eq2Pt(v1+x2+x3,PP);
 when beta5: return Eq2Pt(alpha_1*x1+alpha_2*x2+alpha_3*x3,PP);
 when p+beta1+beta5: return Eq2Pt(v1/alpha_1+alpha_2*x2+alpha_3*x3,PP);
 when beta6: return Eq2Pt(alphaprime_1*x1+alphaprime_2*x2+alphaprime_3*x3,PP);
 when p+beta1+beta6: return Eq2Pt(v1/alphaprime_1+alphaprime_2*x2+alphaprime_3*x3,PP);
 when beta7: return Eq2Pt(alphabis_1*x1+alphabis_2*x2+alphabis_3*x3,PP);
 when p+beta1+beta7: return Eq2Pt(v1/alphabis_1+alphabis_2*x2+alphabis_3*x3,PP);
 when p+beta2+beta4: return Eq2Pt(x1+v2+x3,PP);
  when p+beta3+beta4: return Eq2Pt(x1+x2+v3,PP);
 when p+beta2+beta5: return Eq2Pt(alpha_1*x1+v2/alpha_2+alpha_3*x3,PP);
 when p+beta3+beta5: return Eq2Pt(alpha_1*x1+alpha_2*x2+v3/alpha_3,PP);
 when p+beta2+beta6: return Eq2Pt(alphaprime_1*x1+v2/alphaprime_2+alphaprime_3*x3,PP);
 when p+beta3+beta6: return Eq2Pt(alphaprime_1*x1+alphaprime_2*x2+v3/alphaprime_3,PP);
 when p+beta2+beta7: return Eq2Pt(alphabis_1*x1+v2/alphabis_2+alphabis_3*x3,PP);
 when p+beta3+beta7: return Eq2Pt(alphabis_1*x1+alphabis_2*x2+v3/alphabis_3,PP);
 when p+beta6+beta7: return Eq2Pt(x1/(1-alpha_2*alpha_3)+x2/(1-alpha_3*alpha_1)+x3/(1-alpha_1*alpha_2),PP);
 when p+beta7+beta5: return Eq2Pt(x1/(1-alphaprime_2*alphaprime_3)+x2/(1-alphaprime_3*alphaprime_1)+x3/(1-alphaprime_1*alphaprime_2),PP);
 when p+beta5+beta6: return Eq2Pt(x1/(1-alphabis_2*alphabis_3)+x2/(1-alphabis_3*alphabis_1)+x3/(1-alphabis_1*alphabis_2),PP);
 when p+beta4+beta5: return Eq2Pt(v1/(alpha_1*(1-alpha_2*alpha_3))+v2/(alpha_2*(1-alpha_3*alpha_1))+v3/(alpha_3*(1-alpha_1*alpha_2)),PP);
 when p+beta4+beta6: return Eq2Pt(v1/(alphaprime_1*(1-alphaprime_2*alphaprime_3))+v2/(alphaprime_2*(1-alphaprime_3*alphaprime_1))+v3/(alphaprime_3*(
1-alphaprime_1*alphaprime_2)),PP);
 when p+beta4+beta7: return Eq2Pt(v1/(alphabis_1*(1-alphabis_2*alphabis_3))+v2/(alphabis_2*(1-alphabis_3*alphabis_1))+v3/(alphabis_3*
(1-alphabis_1*alphabis_2)),PP);
end case;
end function;

function Bitangents(D,L,PP)
p:=D.0;
beta1:=D.1+D.2+D.3+D.4+D.5+D.6;
beta2:=D.3+D.5+D.6;
beta3:=D.2+D.3+D.6;
beta4:=D.1+D.3+D.4;
beta5:=D.1+D.4+D.6;
beta6:=D.1+D.2+D.5;
beta7:=D.2+D.4+D.5;
return { Bitangent(beta1,L,PP),Bitangent(beta2,L,PP),Bitangent(beta3,L,PP),
        Bitangent(p+beta2+beta3,L,PP),Bitangent(p+beta3+beta1,L,PP),Bitangent(p+beta1+beta2,L,PP),Bitangent(beta4,L,PP),
		Bitangent(p+beta1+beta4,L,PP),
        Bitangent(beta5,L,PP),Bitangent(p+beta1+beta5,L,PP),Bitangent(beta6,L,PP),
		Bitangent(p+beta1+beta6,L,PP),Bitangent(beta7,L,PP),
        Bitangent(p+beta1+beta7,L,PP),Bitangent(p+beta2+beta4,L,PP),
		Bitangent(p+beta3+beta4,L,PP),Bitangent(p+beta2+beta5,L,PP),
        Bitangent(p+beta3+beta5,L,PP),Bitangent(p+beta2+beta6,L,PP),
		Bitangent(p+beta3+beta6,L,PP),Bitangent(p+beta2+beta7,L,PP),
        Bitangent(p+beta3+beta7,L,PP),Bitangent(p+beta6+beta7,L,PP),
		Bitangent(p+beta7+beta5,L,PP),Bitangent(p+beta5+beta6,L,PP),
        Bitangent(p+beta4+beta5,L,PP),Bitangent(p+beta4+beta6,L,PP),
		Bitangent(p+beta4+beta7,L,PP)};
end function;

/// Given a list of 4 bitangents and a set of 4 bitangents, compute the isomorphisms to pass from the first to the second

function IsomorphismsFromUnorderedBitagents(B1,B2,PP)
IsoList:=[];
Per:=Permutations(B2);
for P in Per do
IsoList:=IsoList cat [PlaneIso(B1,P,PP)];
end for;
  return IsoList;
end function;

/* Given two plane quartics C1,C2 by their Weber's moduli L1,L2, compute all isomorphisms from 
C1 to C2. It uses the 315 quadruples of Tetrads and for each of them there are 4! permutations
 so 315*24=7560 trials and for each a maximum of 28 checkings. 
One can do a bit better if one knows the number of isomorphisms one is supposed to find (parameter order)
*/

function QuarticIsomorphismsFromModuli(L1,L2 : order:=-1) 
	D:=AbelianGroup([2,2,2,2,2,2]);
	F:=Parent(L1[1]);
	L1b:=[F!s : s in L1];
	L2b:=[F!s : s in L2];
	PP:=ProjectiveSpace(F,2);
	Tetrads:=SyzygeticTetrads(D);
	BB:=Bitangents(D,L2b,PP);
//	B1:={PP![1,0,0],PP![0,1,0],PP![0,0,1],PP![1,1,1]};
	B1:={Bitangent(D.1+D.2+D.3+D.4+D.5+D.6,L1b,PP),Bitangent(D.2+D.5,L1b,PP),
	Bitangent(D.3+D.5+D.6,L1b,PP),Bitangent(D.1+D.4+D.5,L1b,PP)};
	testbitangents:={PP![0,0,1],PP![1,1,1],Bitangent(D.1+D.4+D.6,L1b,PP),
		Bitangent(D.1+D.2+D.5,L1b,PP),Bitangent(D.2+D.4+D.5,L1b,PP)};
	PossibleIsoList:={};
	for tetrad in Tetrads do
  	  	B2:={ Bitangent(t,L2b,PP) : t in tetrad };
		M:=Matrix([[b[1],b[2],b[3]] : b in B2]); 
		if Rank(M) eq 3 then
			IsoList:=IsomorphismsFromUnorderedBitagents(B1,B2,PP);
  	  		for iso in IsoList do
				imalist:={iso(testbitangent) : testbitangent in testbitangents};
  	  			if imalist subset BB then 
					PossibleIsoList:=PossibleIsoList join {iso};
					if #PossibleIsoList eq order then 
						return [Matrix(R) :  R in PossibleIsoList];
					end if;
				end if;
  			end for;
		end if;
	end for;
	return [Matrix(R) :  R in PossibleIsoList];
end function;





