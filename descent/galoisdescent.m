// needs IsGL2GeometricEquivalent from descent/quartic_reconstruction-master/magma/g3twists_v2-0/isgl2equiv.m
AttachSpec("vendor/quartic_reconstruction/magma/spec");
AttachSpec("vendor/quartic_isomorphisms/magma/spec");
load "descent/IsomorphismFromBitangents.m";
load "descent/Twistsg3.m";

  /* Given a finite field F of size q = p^e and a matrix M defined over an extension of F,
  this function computes an invertible matrix A such that A*FrobeniusImage(A,e)^(-1)=Mb.
   Where Mb = 1/X*M satisfies Mb*FrobeniusImage(Mb,e)*...*FrobeniusImage(Mb,e)^m-1 ..  = Id for a certain m
  */

  ComputationInvertibleMatrix := function(M,F)
    Fs := BaseRing(M);               
    N := Nrows(M);
    r := Degree(Fs);
    e := Degree(F); 
    repeat
      Ab := RandomMatrix(Fs,N,N);
      A := Ab;
      for i in [1 ..(r div e)-1] do   
        Ab := M*FrobeniusImage(Ab,e);
        A :=  A + Ab;
      end for;
    until IsUnit(A);
    return A;
  end function;


  /*Compute the GaloisDescent of a plane quartic over a finite field over its field of moduli.  Caution: this function takes advantage that all isomorphisms with the conjugates are going to be defined over the field because all bitangents are and the isomorphism has to send bitangents on bitangents. If it is not the case the function QuarticIsomorphisms should use the option geometric:=true
  */

function GaloisDescent(D,FieldOfModuli : Weber:=[], order:=-1)
	PP:=AmbientSpace(D);
	P:=CoordinateRing(PP);
	Dr:=DefiningEquation(D);
	F:=BaseRing(D);
	m:=Degree(F);
 	n:=Degree(FieldOfModuli);
	p:=Characteristic(F);
    phi:=hom< F -> F | F.1^(p^n)>;
    psi:=hom< P -> P | phi,P.1,P.2,P.3>;
	if #Weber eq 0 then 
		Dt:=psi(Dr);
		bool,Iso:=QuarticIsomorphisms(Dr,Dt); 
	else
		Webert:=[phi(w) : w in Weber];
		Iso:=QuarticIsomorphismsFromModuli(Weber,Webert : order:=order);
		if #Iso eq 0 then 
			bool:=false;
		else
			bool:=true;
		end if;
	end if;
	if bool eq false then
		return false;
	end if;
	Iso:=[Transpose(M)^(-1) : M in Iso];
	Iso:=[M/NonZeroElement(M) : M  in Iso];
	OrderList:=[OrderAutomorphism(M,FieldOfModuli) : M in Iso];
	min,pos:=Min([ord[1] : ord in OrderList]);
  	M:=Iso[pos];
	F2:=ext<FieldOfModuli | min>;
	Embed(F2,F);
    M2:=ChangeRing(M,F2);
	D2:=ChangeRing(D,F2);
	Dr2:=DefiningEquation(D2);
	PP2:=AmbientSpace(D2);
	P2:=CoordinateRing(PP2);
 //   phi2:=hom< F2 -> F2 | F2.1^(p^n)>;
  //  psi2:=hom< P2 -> P2 | phi2,P2.1,P2.2,P2.3>;
//	bool,Iso2:=QuarticIsomorphisms(Dr2,Dt2); 
//	Iso2:=[Transpose(M)^(-1) : M in Iso2];
//	OrderList2:=[OrderAutomorphism(M,FieldOfModuli) : M in Iso2];
//	min2,pos2:=Min([ord[1] : ord in OrderList2]);
 // 	M2:=Iso2[pos2];
	OrderList2:=OrderAutomorphism(M2,FieldOfModuli);
	coef:=OrderList2[2];
//	coef:=OrderList2[pos2][2];
	bool,lambda:=NormEquation(F2,FieldOfModuli!coef);
	M3:=M2/lambda;
	A:=ComputationInvertibleMatrix(M3,FieldOfModuli);
    X:=Automorphism(PP2,A);
	return ChangeRing(X(D2), FieldOfModuli),A;
end function;


/* compute the Galois descent of a curve over its field of moduli. Require that all isomorphisms between the curve and its conjugates are defined over the field. Also require the characteristic to be different from 2 and 3 because we use Dixmier-Ohno invariants to compute the field of moduli. If the field of moduli is known, it can be enter as a parameter */

QuarticGaloisDescent:=function(D : twists:=true, FieldOfModuli:=false, Weber:=[], order:=-1)
	Dr:=DefiningPolynomial(D);
	if Type(FieldOfModuli) ne FldFin then
		DO,w:=DixmierOhnoInvariants(Dr);
 		if DO[1] ne 0 then
 			DOn:=[DO[i]/DO[1]^(w[i] div w[1]) : i in [1..13]];
 		else
 			DOn:=[DO[i]^(Lcm(w[i],w[13]) div w[i])/DO[13]^(Lcm(w[i],w[13]) div w[13]) : i  			in [1..13]];
		end if;
		extension:=Lcm([Degree(MinimalPolynomial(I)) : I in DOn]);
		p:=Characteristic(Parent(DOn[1]));
  		FieldOfModuli:=GF(p^extension);
  	DO:=[FieldOfModuli!I : I in DOn];
	end if;
  	C,A:=GaloisDescent(D,FieldOfModuli : Weber:=Weber,order:=order);
  	kk<x,y,z>:=Ambient(C); // name the variables
	if twists then		
		if #Weber eq 0 then
			bool,Aut:=QuarticIsomorphisms(Dr,Dr);
		else
			Aut:=QuarticIsomorphismsFromModuli(Weber,Weber : order:=order);
		end if;
		Aut:=[Transpose(M) : M in Aut];
		Aut2:=[A^(-1)*M*A : M  in Aut];
		T:=QuarticTwists(C,Aut2);
		for C in T do
		  kk<x,y,z>:=Ambient(C);
		end for;
		return T;
	end if;
	return [C];
end function;	

	
