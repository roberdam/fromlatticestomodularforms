
// This function return a list [*m, lamda*] such that
// FrobeniusImage(M,e)^m-1 .. FrobeniusImage(M,e)M = lambda Id
// Where M is a NXN matrix defined over the finite field Fs, #Fs = q^r.
// F is the field other which my curve is defined. #F = p^e = q
// FrobeniusImage(M,e)^m = FrobeniusImage(..FrobeniusImage(M,e)..,e) m times.

OrderAutomorphism := function(M,FieldOfModuli)
  e := Degree(FieldOfModuli);
  H := M;
  P := M;
  m := 1;
  while not IsScalar(P) do
    P := H*FrobeniusImage(P,e);
    m := m+1;
  end while;
 return [*m, P[1, 1]*];
end function;



//This function return a non zero element of the NxN matrix M

NonZeroElement := function(M)
  N := Nrows(M);
  for j in [1 .. N] do
    for i in [1 .. N] do
      if M[i,j] ne 0 then
        return M[i,j];
      end if;
    end for;
  end for;
  printf("Error, your matrix is zero");
  return 0;
end function;

// Given the reduced automorphism group G and the finite field F,
// this function compute it cohomology classes.

CohomologyClass := function(G,F)
  L := G;
  e := Degree(F);
  CohoClass := [**];
  while not IsEmpty(L) do
    Append(~CohoClass,L[1]);
    if not IsEmpty(L) then
        for i in [1 .. #G] do
        EqClassCoho := FrobeniusImage(G[i],e)*CohoClass[#CohoClass]*G[i]^(-1);
		Exclude(~L,1/NonZeroElement(EqClassCoho)*EqClassCoho);
        end for;
   end if;
  end while;
  return CohoClass;
end function;



/*Given a finite field F of size q = p^e and a matrix M defined over an extension of F,
 this function compute an invertible matrix A such that FrobeniusImage(A,e)^(-1)*A=Mb.
 Where Mb = 1/X*M verify FrobeniusImage(Mb,e)^m-1 .. FrobeniusImage(Mb,e)Mb = Id
*/

ComputationInvertibleMatrix2 := function(M,F)
  Fs := BaseRing(M);              
  N := Nrows(M);
  r := Degree(Fs);
  e := Degree(F); 
  L := OrderAutomorphism (M,F);
  m := L[1] ;
  K := ext<Fs | m>;  
  _,X := NormEquation(K,L[2]);
  Mb := ChangeRing(M,K)*1/X;
  repeat
    Ab := RandomMatrix(K,N,N);
    A := Ab;
    for i in [1 ..r*m-1] do   
      Ab := Mb*FrobeniusImage(Ab,e);
      A :=  A + Ab;
    end for;
  until IsUnit(A);
// in this step we have computed A such that A*FrobeniusImage(A,e)^(-1)=Mb
  return A;
end function;



/* Given a projective curve C and its Automorphism group Aut (given by a list of matrix
 N x N), this function compute the twists of C
*/

QuarticTwists:= function(C,Aut)
	F := BaseRing(C);
	Aut2:=[1/NonZeroElement(g)*g : g in Aut];
	Coh := CohomologyClass(Aut2,F);
	T := []; 
	f := DefiningPolynomial(C);
	for M in Coh do
    A := ComputationInvertibleMatrix2(M,F);
	F2:=BaseRing(A);
	C2:=ChangeRing(C,F2);
	X2:=Automorphism(AmbientSpace(C2),A);
    Append(~T,ChangeRing(X2(C2),F));
  end for;
  return T;
end function;


