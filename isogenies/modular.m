import "vendor/avisogenies/src/libav.m" : SumOfSquares;

/* Tools to compute modular factors */

// Weight is the integral weight of the modular function; l is the degree
// of the isogeny
function CorrectingFactor(theta_alg, l, weight)
  // D:=theta_alg`numbering;
  // g:=#Invariants(D);
  correcting_factor:=theta_alg`coord[Universe(Keys(theta_alg`coord)).0];
  u := SumOfSquares(l);
  if #u eq 1 then
    return 1;
  elif #u eq 2 then
    return correcting_factor^(weight);
  elif #u eq 4 then
    return correcting_factor^( (weight * 6) div 4);
  end if;
end function;

/* Genus 2 moduli */

function Chi10(theta_alg, l)
  theta_analytic:=GetAnalyticThetaNullPoint(theta_alg);
  coord:=theta_analytic`coord;
  prodevencoord:=1;
  nbevencoord:=0;
  for i in Keys(coord) do
    if coord[i] ne 0 then
      prodevencoord*:=coord[i];
      nbevencoord+:=1;
    end if;
  end for;
  if nbevencoord ne 10 then
    return 0;
  else
    // In level 2 we already deal with squares
    if not theta_alg`l2 then
      prodevencoord := prodevencoord^2;
    end if;
    return prodevencoord / CorrectingFactor(theta_alg, l, 10);
  end if;
end function;

function Genus2Chi10(theta, Ker)
  l:=Ker[2]; gens:=Ker[1];
  E:=Curve(gens[1][1]); F:=BaseRing(E);
  chi10 := F! Chi10(theta, l);
  return chi10;
end function;

/* Genus 3 */
function Chi18(theta_alg, l)
  theta_analytic:=GetAnalyticThetaNullPoint(theta_alg);
  coord:=theta_analytic`coord;
  prodevencoord:=1;
  nbevencoord:=0;
  for i in Keys(coord) do
    if coord[i] ne 0 then
      prodevencoord*:=coord[i];
      nbevencoord+:=1;
    end if;
  end for;
  if nbevencoord ne 36 then
    return 0;
  else
    if theta_alg`l2 then
      // In level 2 we return chi_18^2
      return prodevencoord / CorrectingFactor(theta_alg, l, 36);
    else
      return prodevencoord / CorrectingFactor(theta_alg, l, 18);
    end if;
  end if;
end function;

function SerreObstruction(F, theta_alg, l)
  obstruction :=F ! - Chi18(theta_alg, l);
  return  not IsSquare(obstruction), obstruction;
end function;

// theta is one member from IsogeniesFromKernels(Kers); and Ker should be
// the corresponding kernel (we don't really need it, we just use it to
// extract the base field and l)
function Genus3Obstruction(theta, Ker)
  l:=Ker[2]; gens:=Ker[1];
  E:=Curve(gens[1][1]); F:=BaseRing(E);
  obs, moduli:=SerreObstruction(F, theta, l);
  return obs, moduli;
end function;

/* Curves in genus 2 and 3 */
function AllCurves(kernels: filter:=false, l2:=false)
  callback:=function(theta, ker, i)
    gens:=ker[1];
    E:=Curve(gens[1][1]); F:=BaseRing(E); q:=#F; g:=#gens;
    if Category(filter) eq BoolElt then
      if filter then
        target:=1+q-&+[Trace(Curve(gen[1])): gen in gens];
      else
        target:=-1; //don't filter curves
      end if;
    else
      target:=filter;
    end if;
    if g eq 2 then
      chi10 := Genus2Chi10(theta, ker);
      vprintf AVIsogenies, 1: "* Kernel %o -> Chi10: %o\n", i, chi10;
      if chi10 ne 0 then
        curves := AllTwists(theta: NumberOfPoints:=target, FieldOfModuli:=F);
      else
        curves:=[];
      end if;
    elif g eq 3 then
      obs, chi18 := Genus3Obstruction(theta, ker);
      vprintf AVIsogenies, 1: "* Kernel %o -> Chi18: %o, Obstruction: %o\n", i, chi18, obs;
      if obs eq false then
        order:=-1;
        if #ker ge 3 then
          order:=#ker[3][3] div 2;
        end if;
        curves := AllTwists(theta: NumberOfPoints:=target, FieldOfModuli:=F, order:=order);
      else
        curves:=[];
      end if;
    else
      curves:=[]; // TODO: not supported
    end if;
    vprint AVIsogenies, 1: "Curves: ", [<C, #Points(C)>: C in curves];
    return curves;
  end function;
  return IsogeniesFromKernels(kernels: callback:=callback, l2:=l2);
end function;

/* Genus 4 */
// return the Igusa modular form which characterize in g=4 the locus of
// Jacobians over the algebraic closure.
IgusaForm:=function(coord, DDg)
  N1:=Matrix(GF(2),[[0,0,0,1],[1,1,1,0]]);
  N2:=Matrix(GF(2),[[0,0,1,1],[0,0,0,1]]);
  N3:=Matrix(GF(2),[[0,0,1,0],[1,0,1,1]]);
  N:={N1+N1,N1,N2,N3,N1+N2,N1+N3,N2+N3,N1+N2+N3};
  M1:=Matrix(GF(2),[[1,0,1,0],[1,0,1,0]]);
  M2:=Matrix(GF(2),[[0,0,0,1],[1,0,0,0]]);
  M3:=Matrix(GF(2),[[0,0,1,1],[1,0,1,1]]);
  pi1:=&*[coord[FromMToC(M1+n, DDg)] : n in N];
  pi2:=&*[coord[FromMToC(M2+n, DDg)] : n in N];
  pi3:=&*[coord[FromMToC(M3+n, DDg)] : n in N];
  return pi1^2+pi2^2+pi3^2-2*pi1*pi2-2*pi1*pi3-2*pi2*pi3;
end function;

function IgusaModularForm(theta_alg, l)
  theta_analytic:=GetAnalyticThetaNullPoint(theta_alg);
  coord:=theta_analytic`coord; DDg:=Universe(coord);
  return IgusaForm(coord, DDg) / CorrectingFactor(theta_alg, l, 8);
end function;

function Genus4Igusa(theta, Ker)
  l:=Ker[2]; gens:=Ker[1];
  E:=Curve(gens[1][1]); F:=BaseRing(E);
  igusa:= F! IgusaModularForm(theta, l);
  return igusa;
end function;
