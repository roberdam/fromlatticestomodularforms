// transform a matrix 2*g with coefficients in F2 in the corresponding
// characteristic in avisogenies format DDg
FromMToC:=function(M,DDg)
  C:=DDg.0;
  g:=NumberOfColumns(M);
  for i:=1 to g do
    C:=C+Integers()!(M[1][i])*DDg.i;
    C:=C+Integers()!(M[2][i])*DDg.(g+i);
  end for;
  return C;
end function;

Genus2Reconstruction:=function(coord)
  DDg:=Universe(coord);
  M1:=Matrix(GF(2),[[0,1],[0,0]]);
  M2:=Matrix(GF(2),[[0,0],[0,0]]);
  M3:=Matrix(GF(2),[[1,0],[0,0]]);
  M4:=Matrix(GF(2),[[1,1],[0,0]]);
  M5:=Matrix(GF(2),[[0,0],[0,1]]);
  M6:=Matrix(GF(2),[[1,0],[0,1]]);
  lambda1:=(coord[FromMToC(M1, DDg)]*coord[FromMToC(M2,
      DDg)]/(coord[FromMToC(M3, DDg)]*
      coord[FromMToC(M4, DDg)]))^2;
  lambda2:=(coord[FromMToC(M1, DDg)]*coord[FromMToC(M5,
      DDg)]/(coord[FromMToC(M6, DDg)]*
      coord[FromMToC(M4, DDg)]))^2;
  lambda3:=(coord[FromMToC(M2, DDg)]*coord[FromMToC(M5,
      DDg)]/(coord[FromMToC(M6, DDg)]*
      coord[FromMToC(M3, DDg)]))^2;
  FF:=Parent(lambda1);
  P<x>:=PolynomialRing(FF);
  H:=HyperellipticCurve(x*(x-1)*(x-lambda1)*(x-lambda2)*(x-lambda3));
  I:=G2Invariants(H);
  d:=Lcm([Degree(MinimalPolynomial(Iv)) : Iv in I]);
  p:=Characteristic(Parent(I[1]));
  FFm:=GF(p^d);
  I:=[FFm!Iv : Iv in I];
  H0:=HyperellipticCurveFromG2Invariants(I);
  return H0;
end function;

// return all curves (including twists) corresponding to an algebraic theta
function AllTwists(theta_alg: NumberOfPoints:=-1)
  D:=theta_alg`numbering;
  g:=#Invariants(D);
  if g eq 2 then
    if theta_alg`l2 then
      newH, new_abs_inv:=theta0_to_curve(theta_alg);
      if Category(newH) ne BoolElt then
        // we reconstruct the curve up to twists
        g2:=G2Invariants(newH);
        H:=ChangeRing(HyperellipticCurveFromG2Invariants(g2),Universe(new_abs_inv));
      else
        // The curve was decomposable
        return [];
      end if;
    else
      // the code above works in level 4 too, but sometimes it falsely
      // detects some decomposable curves. Use a direct algorithm here.
      theta_an:=GetAnalyticThetaNullPoint(theta_alg);
      H:=Genus2Reconstruction(theta_an`coord);
    end if;
    HT:=Twists(H);
    if NumberOfPoints eq -1 then
      return HT;
    else
      return [Hi : Hi in HT | #Points(Hi) eq NumberOfPoints];
    end if;
  end if;

  //g=3 case
  if g eq 3 then // only works in level 4
    theta_analytic:=GetAnalyticThetaNullPoint(theta_alg);
    // in conversion.m. Return the list of possible descents of C.
    vprint AVIsogenies, 3: "-> Computing the possible descents";
    Clist:=analytic_theta_to_quartic(theta_analytic);
    if NumberOfPoints eq -1 then
      return Clist;
    else
      return [C : C in Clist | #Points(C) eq NumberOfPoints];
    end if;
  end if;
end function;

// Show the zeta functions of the computed curves
procedure InfosOnCurves(curves)
  for T in curves do
    if IsHyperelliptic(T) then
      T, #T, Factorization(LPolynomial(T));
    else
      T, #Points(T), Factorization(Numerator(ZetaFunction(T)));
    end if;
  end for;
end procedure;
