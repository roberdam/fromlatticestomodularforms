import "vendor/avisogenies/src/Arithmetic.m" : init_theta_point, init_theta_null_point, add, compare_points, diff_add;
import "vendor/avisogenies/src/libav.m" : GetCastMinField;
import "vendor/avisogenies/src/rosenhain.m" : theta_null_to_rosenhain;

// Convert an analytic to algebraic theta null point, without
// precomputations as in AnalyticToAlgebraicThetaNullPoint
function RawAnalyticToAlgebraicThetaNullPoint(g,thc,A)
  point:=AssociativeArray(A);
  if thc`level eq 2 then
    for b in A do
      point[b]:=0;
      for a in A do point[b]+:=thc`coord[Eltseq(a) cat Eltseq(b)]; end for;
    end for;
    assert point[A!0] ne 0;
    return point;
  else
    Ab:=thc`numbering;
    As:=AbelianGroup([2 : x in [1..g]]);
    NN:=Integers();
    for b in A do
      point[b]:=0;
      for a in As do
         sign:=(-1)^(&+[NN!(Eltseq(a)[i]*(Eltseq(b)[i]-(Eltseq(b)[i] mod 2))/2) : i in [1..g]]);
         point[b]+:=thc`coord[Ab!(Eltseq(a) cat Eltseq(b))]*sign;
      end for;
    end for;
    return point;
  end if;
end function;

// Convert from algebraic to analytic
function GetAnalyticThetaNullPoint(theta_alg: DDg:=false)
  D:=theta_alg`numbering;
  g:=#Invariants(D);
  if Category(DDg) ne GrpAb then
    DDg:=AbelianGroup([2: i in [1..2*g]]);
  end if;
  return AlgebraicToAnalyticThetaNullPoint(g,theta_alg,DDg);
end function;

//**** converting to theta coordinates ****//
procedure elliptic_theta_null_point(E, ~precomp: init:="isogenies")
  assert E eq WeierstrassModel(E); //be sure we are in short Weierstrass form
  // l2:=precomp["l2"];
  if not IsDefined(precomp, "D1") then
    DD1:=AbelianGroup([2,2]);
    D1:=AbelianGroup([4]);
    precomp["D1"]:=D1; precomp["DD1"]:=DD1;
  end if;

  D1:=precomp["D1"];
  DD1:=precomp["DD1"];
  n:=Invariants(D1)[1];
  g1:=#Invariants(D1);

  F:=BaseField(E);
  f:=HyperellipticPolynomials(E);
  Ros,K:=RootsInSplittingField(f);
  Ros:=[i[1]: i in Ros];
  KK<x>:=PolynomialRing(K);
  alpha:=Ros[1]; beta:=Ros[2]; gamma:=Ros[3];
  r,K:=RootsInSplittingField(x^2-(beta-alpha));
  rac:=r[1,1];

  th:=AssociativeArray();
  // t1:=1/((alpha-beta)*(gamma-beta)); //t1=th[00]
  // t2:=-1/((beta-alpha)*(gamma-alpha)); //t2=th[01]
  // t3:=-1/((beta-gamma)*(alpha-gamma)); //t3=th[10]

  // New formulas with the explicit constant:
  // theta[00]'^4=c*theta[00]^4=e_1-e_3
  // theta[01]'^4=c*theta[01]^4=e_1-e_2
  // theta[10]'^4=c*theta[10]^4=e_2-e_3
  t1:=(alpha-gamma); 
  t3:=(alpha-beta);
  t2:=(beta-gamma);

  KK<x>:=PolynomialRing(K);
  r,K:=RootsInSplittingField(x^n-t1);
  th[DD1.0]:=r[1,1];
  KK<x>:=PolynomialRing(K);
  r,K:=RootsInSplittingField(x^n-t2);
  th[DD1.2]:=r[1,1];
  KK<x>:=PolynomialRing(K);
  r,K:=RootsInSplittingField(x^n-t3);
  th[DD1.1]:=r[1,1];
  //th[DD1.1]^4:=-th[DD1.0]^4*th[DD1.1]^4;
  th[DD1.1+DD1.2]:=0;
  theta0_an:=AnalyticThetaNullPoint(n,th,DD1,g1: level2 := n eq 2);

  precompE:=AssociativeArray();
  precompE["E"]:=E;
  precompE["Ros"]:=Ros; precompE["rac"]:=rac;
  precompE["theta0_an"]:=theta0_an;
  // theta0:=AnalyticToAlgebraicThetaNullPoint(g1,theta0_an,D1);
  // precompE["theta0"]:=theta0;
  theta0:=RawAnalyticToAlgebraicThetaNullPoint(g1,theta0_an,D1);
  // print init;
  precompE["theta0"]:=init_theta_null_point(theta0: init:=init);
  if not IsDefined(precomp,"E") then
    precomp["E"]:=AssociativeArray();
  end if;
  precomp["E"][E] := precompE;
end procedure;

function elliptic_point_to_theta(P,precomp)
  E:=Curve(P);
  precompE:=precomp["E"][E];
  D1:=precompE["theta0"]`numbering;
  n:=Invariants(D1)[1]; g1:=#Invariants(D1);
  l2:= n eq 2;
  if P eq precompE["E"]!0 then
    return precompE["theta0"];
  end if;
  xy:=Eltseq(P)[1..2];
  if l2 then
    Pan:=MumfordToLevel2ThetaPoint(g1,precompE["Ros"],precompE["theta0_an"],[xy]);
  else
    Pan:=MumfordToLevel4ThetaPoint(g1,precompE["Ros"],precompE["rac"],precompE["theta0_an"],[xy]);
  end if;
  return AnalyticToAlgebraicThetaPoint(g1,Pan,D1);
end function;

//// Segre embedding //////////////
function segre(P: Dg:=false)
  if Category(Dg) ne GrpAb then
    D1:=P[1]`numbering;
    n:=Invariants(D1)[1];
    Dg:=AbelianGroup([n: i in [1..#P]]);
  end if;
  g:=#Invariants(Dg);
  r:=AssociativeArray();
  for i in Dg do
    ii:=Eltseq(i);
    r[i]:=&*[P[z]`coord[P[z]`numbering!ii[z]]: z in [1..g]];
  end for;
  return r;
end function;

function unsegre(P: D1:=false)
  D:=P`numbering;
  g:=#Invariants(D);
  if Category(D1) ne GrpAb then
    D1:=AbelianGroup(Invariants(D)[1]);
  end if;
  r:=[];
  for z in [1..g] do
    rz:=AssociativeArray();
    for i in D1 do
      ii:=Rep(Eltseq(i));
      rz[i]:=P`coord[ii*D.z];
    end for;
  r[z]:=rz;
  end for;
  return r;
end function;

function elliptic_points_to_theta(P,precomp)
  Dg:=precomp["D"]["D"];
  return init_theta_point(segre(
    <elliptic_point_to_theta(P[i], precomp): i in [1..#P]>: Dg:=Dg));
end function;

/*
//from the basis of K, return \sum el[i] K[i] in theta coordinates
// here the basis is given by tuples
function element_basis_to_theta(el,K,precomp)
  el:=Eltseq(el); g:=#el;
  return init_theta_point(segre(
          <point_to_theta(
            &+[el[i]*K[i][z]: i in [1..#K]],precomp): z in [1..g]>
        ,precomp));
end function;
*/

//// From theta to genus 3 and genus 2 curve //////////////
// genus 3
function analytic_theta_to_weber_alphai(th)
  t:=th`coord; D:=th`numbering;
  t0:=t[D.0];
  K:=Parent(t0);
  KK<x>:=PolynomialRing(K);
  roots:=[z[1]: z in Roots(x^4-1)];
  i:=Rep([z: z in roots | z^2 eq K!-1]);

  alpha_1:=i*t[D.1+D.6]*t[D.4+D.6]/(t[D.1+D.3]*t[D.3+D.4]);
  alphaprime_1:=i*t[D.4+D.6]*t[D.1+D.2+D.4+D.5]/(t[D.2+D.3+D.5+D.6]*t[D.1+D.3]);
  alphabis_1:=-t[D.1+D.2+D.4+D.5]*t[D.1+D.6]/(t[D.3+D.4]*t[D.2+D.3+D.5+D.6]);

  alpha_2:=i*t[D.2+D.4+D.6]*t[D.1+D.2+D.6]/(t[D.2+D.3+D.4]*t[D.1+D.2+D.3]);
  alphaprime_2:=i*t[D.1+D.2+D.6]*t[D.5]/(t[D.1+D.3+D.4+D.5+D.6]*t[D.2+D.3+D.4]);
  alphabis_2:=t[D.5]*t[D.2+D.4+D.6]/(t[D.1+D.2+D.3]*t[D.1+D.3+D.4+D.5+D.6]);

  alpha_3:=i*t[D.4+D.5+D.6]*t[D.1+D.5+D.6]/(t[D.3+D.4+D.5]*t[D.1+D.3+D.5]);
  alphaprime_3:=i*t[D.1+D.5+D.6]*t[D.2]/(t[D.1+D.2+D.3+D.4+D.6]*t[D.3+D.4+D.5]);
  alphabis_3:=t[D.2]*t[D.4+D.5+D.6]/(t[D.1+D.3+D.5]*t[D.1+D.2+D.3+D.4+D.6]);

  return [alpha_1, alphaprime_1, alphabis_1,
    alpha_2, alphaprime_2, alphabis_2,
    alpha_3, alphaprime_3, alphabis_3];
end function;

//// Given Weber's moduli L return a model of the curve
function RiemannModel(L)
  F:=Parent(L[1]);
  P<X,Y,Z>:=PolynomialRing(F,3);
  M:=-Matrix([[L[1],L[4],L[7]],[L[2],L[5],L[8]],[L[3],L[6],L[9]]]);
  Mp:=Matrix([[1/L[1],1/L[4],1/L[7]],[1/L[2],1/L[5],1/L[8]],[1/L[3],1/L[6],1/L[9]]]);
  N:=Mp^(-1)*M;
  v1:=(N[1,1]*X+N[1,2]*Y+N[1,3]*Z);
  v2:=(N[2,1]*X+N[2,2]*Y+N[2,3]*Z);
  v3:=(N[3,1]*X+N[3,2]*Y+N[3,3]*Z);
  //vprintf AVIsogenies, 3: "  RiemannModel: %o\n", (v1+v2+v3)+X+Y+Z;
  return (X*v1+Y*v2-Z*v3)^2-4*X*v1*Y*v2;
end function;

function analytic_theta_to_quartic(theta)
  weber:=analytic_theta_to_weber_alphai(theta);
  Dr:=RiemannModel(weber);
  D:=Curve(ProjectiveSpace(Parent(Dr)),Dr);
  return D, weber;
end function;

// genus 2
function level4_to_level2_iso(theta) // NOT USED
  D:=theta`numbering;
  D0:=sub<D | [2*D.i: i in [1..#Invariants(D)]]>;
  r:=AssociativeArray();
  for i in D0 do
    r[i]:=theta`coord[i];
  end for;
  return init_theta_null_point(r);
end function;

function level4_to_level2(theta)
  D:=theta`numbering;
  g:=#Invariants(D);
  DDg:=AbelianGroup([2: i in [1..2*g]]);
  thetaan:=AlgebraicToAnalyticThetaNullPoint(g,theta,DDg);
  r:=AssociativeArray();
  for i in DDg do
    r[i]:=thetaan`coord[i]^2;
  end for;
  thetaan2:=AnalyticThetaNullPoint(4,r,DDg,g: level2 := true);
  D0:=AbelianGroup([2: i in [1..g]]);
  return AnalyticToAlgebraicThetaNullPoint(g,thetaan2,D0);
end function;

// Convert an algebraic theta null point to an hyperelliptic curve in genus 2
// using level 2 theta constants
function genus2_theta0_to_curve(theta)
  if theta`l2 then
    theta0:=theta;
  else
    theta0:=level4_to_level2(theta);
  end if;
  try 
    ros:=theta_null_to_rosenhain(theta0);
    K<x>:=PolynomialRing(Universe(ros));
    f:=&*[x-i: i in ros];
    newH:=HyperellipticCurve(f);
  catch e
    // if there is an error it probably means that the variety was not
    // indecomposable
    "CAUGHT ERROR:", e;
    return false;
  end try;
  return newH;
  /* // This is now done in theta_to_curve
  vprint AVIsogenies, 3: "Absolute invariants";
  vtime AVIsogenies, 3: newAI:=AbsoluteInvariants(newH);
  Inv:=GetCastMinField(newAI); 
  H:=HyperellipticCurveFromG2Invariants(Inv[1..3]);
  return H,Inv[1..3]; */
end function;

// transform a matrix 2*g with coefficients in F2 in the corresponding
// characteristic in avisogenies format DDg
FromMToC:=function(M,DDg)
  C:=DDg.0;
  g:=NumberOfColumns(M);
  for i:=1 to g do
    C:=C+Integers()!(M[1][i])*DDg.i;
    C:=C+Integers()!(M[2][i])*DDg.(g+i);
  end for;
  return C;
end function;

// Alternative genus 2 reconstruction directly in level 4
Genus2Reconstruction:=function(coord)
  DDg:=Universe(coord);
  M1:=Matrix(GF(2),[[0,1],[0,0]]);
  M2:=Matrix(GF(2),[[0,0],[0,0]]);
  M3:=Matrix(GF(2),[[1,0],[0,0]]);
  M4:=Matrix(GF(2),[[1,1],[0,0]]);
  M5:=Matrix(GF(2),[[0,0],[0,1]]);
  M6:=Matrix(GF(2),[[1,0],[0,1]]);
  den1:=coord[FromMToC(M3, DDg)]* coord[FromMToC(M4, DDg)];
  den2:=coord[FromMToC(M6, DDg)]*coord[FromMToC(M4, DDg)];
  den3:=coord[FromMToC(M6, DDg)]* coord[FromMToC(M3, DDg)];
  if den1 eq 0 or den2 eq 0 or den3 eq 0 then
    return false;
  end if;
  lambda1:=(coord[FromMToC(M1, DDg)]*coord[FromMToC(M2, DDg)]/den1)^2;
  lambda2:=(coord[FromMToC(M1, DDg)]*coord[FromMToC(M5, DDg)]/den2)^2;
  lambda3:=(coord[FromMToC(M2, DDg)]*coord[FromMToC(M5, DDg)]/(den3))^2;
  FF:=Parent(lambda1); P<x>:=PolynomialRing(FF);
  H:=HyperellipticCurve(x*(x-1)*(x-lambda1)*(x-lambda2)*(x-lambda3));
  return H;
end function;

// Convert an algebraic theta to a curve in genus 2 and 3, and coerce it to
// its field of moduli
function theta_to_curve(theta_alg: FieldOfModuli := true)
  D:=theta_alg`numbering; g:=#Invariants(D);
  if g eq 2 then
    if theta_alg`l2 then
      newH:=genus2_theta0_to_curve(theta_alg);
    else
      // the code above works in level 4 too, but sometimes it falsely
      // detects some decomposable curves. Use a direct algorithm here.
      theta_an:=GetAnalyticThetaNullPoint(theta_alg);
      newH:=Genus2Reconstruction(theta_an`coord);
    end if;
    if Category(newH) ne BoolElt then
      if Category(FieldOfModuli) eq BoolElt then
        if FieldOfModuli then
          // we reconstruct the curve up to twists
          I:=G2Invariants(newH);
          d:=Lcm([Degree(MinimalPolynomial(Iv)) : Iv in I]);
          p:=Characteristic(Parent(I[1])); FFm:=GF(p^d);
          I:=[FFm!Iv : Iv in I];
          H:=HyperellipticCurveFromG2Invariants(I);
          // H:=ChangeRing(HyperellipticCurveFromG2Invariants(g2),Universe(I));
          return H;
        else
          vprint AVIsogenies, 3: "  Warning: the curve is decomposable!";
          return newH;
        end if;
      else // assume FieldOfModuli is a field
        I:=G2Invariants(newH); I:=[FieldOfModuli!Iv : Iv in I];
        H:=HyperellipticCurveFromG2Invariants(I);
        return H;
      end if;
    else
      // The curve was decomposable
      return false;
    end if;
  elif g eq 3 then // only works in level 4
    theta_analytic:=GetAnalyticThetaNullPoint(theta_alg);
    // warning: here we don't descend to the field of moduli
    // this will be done by QuarticGaloisDescent in AllTwists
    return analytic_theta_to_quartic(theta_analytic);
  end if;
end function;

// return all curves (including twists) corresponding to an algebraic theta
// order is the order of the automorphism group. If available, this speeds
// up descent in genus 3
// If NumberOfPoints is given, we restrict to the curves of this cardinal
function AllTwists(theta_alg: NumberOfPoints:=-1, FieldOfModuli:=true, order:=-1)
  D:=theta_alg`numbering; g:=#Invariants(D);
  if g eq 2 then
    C:=theta_to_curve(theta_alg: FieldOfModuli:=FieldOfModuli);
    if Category(C) ne BoolElt then
      Clist:=Twists(C);
    else
      Clist:=[];
    end if;
  elif g eq 3 then // only works in level 4
    C, weber:=theta_to_curve(theta_alg: FieldOfModuli:=FieldOfModuli);
    //  Clist:=DescentCurve(D,1,Degree(BaseRing(D)));
    Clist:=QuarticGaloisDescent(C: FieldOfModuli := FieldOfModuli);
    // the fast method failed, switch to the slow method
    if Category(Clist) eq BoolElt then
      vprint AVIsogenies, 6: "  Warning: switching to the slow method to compute descent, order=", order;
      Clist:=QuarticGaloisDescent(C: FieldOfModuli := FieldOfModuli, Weber:=weber, order:=order);
    end if;
  end if;
  if NumberOfPoints eq -1 then
    return Clist;
  else
    return [C : C in Clist | #Points(C) eq NumberOfPoints];
  end if;
end function;

// Show the zeta functions of the computed curves
procedure InfosOnCurves(curves)
  for T in curves do
    if IsHyperelliptic(T) then
      T, #T, Factorization(LPolynomial(T));
    else
      T, #Points(T), Factorization(Numerator(ZetaFunction(T)));
    end if;
  end for;
end procedure;
