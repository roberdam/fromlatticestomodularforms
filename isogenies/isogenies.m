import "vendor/avisogenies/src/Isogenies.m" : Isogenie, precompute_isogenies;
import "vendor/avisogenies/src/Arithmetic.m" : precompute_D;

function pairing_on_tuple(l,t1,t2)
  assert #t1 eq #t2;
  return &*[WeilPairing(t1[i],t2[i],l): i in [1..#t1]];
end function;

function TestKernel(l,K)
  g:=#Rep(K); gg:=#K;
  if g ne gg then
    vprint AVIsogenies, 1: "In TestKernel, the dimension is", g, "but the number of generators is", gg;
    return false;
  end if;
  Elist:=[Curve(g): g in Rep(K)];
  for P in K do
    Elist2:=[Curve(g): g in P];
    if Elist ne Elist2 then
      vprint AVIsogenies, 1: "In TestKernel, the points do not all live in the same variety!";
    end if;
  end for;

  //"Testing if we have an isotropic kernel";
  //This should always be the case if the kernel comes from "enumeration-max-isotropic-subgroup2.magma";
   for i in [1..g-1] do
     for j in [i+1..g] do
       if pairing_on_tuple(l,K[i],K[j]) ne 1 then return false; end if;
    end for;
  end for;
  //"Testing if we have a rational kernel";
  //E:=ChangeRing(Curve(Rep(K)[1]),F);
  E:=Curve(Rep(K)[1]);
  //I did not find an easier way to compute the Frobenius on an elliptic
  //curve.... sigh
  Fr:=FrobeniusMap(E,Degree(BaseField(E)));
   for i in [1..g] do
     P:=K[i]; EF:=Parent(P[1]); PFr:=<EF!Fr(z): z in P>;
     for j in [1..g] do
       if pairing_on_tuple(l,PFr,K[j]) ne 1 then return false; end if;
    end for;
  end for;
  return true;
end function;


function precompute_E(Elist: l2:=false, init:="isogenies")
  g:=#Elist;
  vprint AVIsogenies, 2: "* In precompute_E";
  DD1:=AbelianGroup([2,2]);
  if l2 then
    D1:=AbelianGroup([2]);
    Dg:=AbelianGroup([2: i in [1..g]]);
  else
    D1:=AbelianGroup([4]);
    Dg:=AbelianGroup([4: i in [1..g]]);
  end if;
  precomp:=AssociativeArray();
  precomp["D1"]:=D1; precomp["DD1"]:=DD1;
  precomp["E"]:=AssociativeArray();

  // precomp["l2"]:=l2;
  // precomp["g"]:=g;

  vprint AVIsogenies, 3: "-> precompute_Dg";
  precompute_D(Dg,~precomp);

  vprint AVIsogenies, 3: "-> compute the elliptic theta null points";
  thetas:=[];
  for i in [1..g] do
    E:=Elist[i];
    if not IsDefined(precomp["E"], E) then
      elliptic_theta_null_point(E, ~precomp);
    end if;
    theta0:=precomp["E"][E]["theta0"];
    Append(~thetas, theta0);
  end for;

  vprint AVIsogenies, 3: "-> compute the product theta null point";
  vtime AVIsogenies, 3: P0:=init_theta_null_point(segre(<thetas[i]: i in [1..g]>: Dg:=Dg): precomp:=precomp, init:=init);
  // what's taking a long time is actually precompute_riemann here
  precomp["P0"]:=P0;
  return P0,precomp;
end function;

function GetIsogeny(l, K, precomp)

  Dg:=precomp["D"]["D"];
  g:=#Invariants(Dg);
  vprint AVIsogenies, 2: "* In GetIsogeny: Computing one kernel of degree", l, "in genus", g;
  assert TestKernel(l,K);

  vtime AVIsogenies, 3: precompute_isogenies(Dg,l,~precomp);
  Dl:=Universe(precomp["l"]["gen"]);

  L:=AssociativeArray();

  P0:=precomp["P0"];
  vprint AVIsogenies, 3: "-> compute theta coordinates of points in K";
  L[Dl.0]:=P0;
  for i in [1..g] do
    L[Dl.i]:=elliptic_points_to_theta(K[i], precomp);
  end for;
  for i in [1..g-1] do
    for j in [i+1..g] do
      L[Dl.i+Dl.j]:=elliptic_points_to_theta(<K[i][z]+K[j][z]: z in [1..g]>, precomp);
    end for;
  end for;
  // precomp["L"]:=L;

  vprint AVIsogenies, 3: "-> computing the isogeny";
  theta_alg, _L:=Isogenie(L,precomp);
  return theta_alg, precomp;
end function;

// Take a list of kernels, and return the (algebraic) theta null points
function IsogeniesFromKernels(Kerlist: l2:=false, precomp:=false, init:="isogenies", callback:=false)
  if Category(Kerlist) eq Tup then
    // alllow to pass just one kernel rather than a list
    Kerlist:=[Kerlist];
  end if;
  vprint AVIsogenies, 1: "** Computing", #Kerlist, "isogenies";
  i:=0;
  r:=[* *];
  last_Elist:=[]; precompl:=AssociativeArray();

  for Ker in Kerlist do
    i:=i+1; vprint AVIsogenies, 2: "=> Kernel n°", i;
    // Ker is of the form <Kernel, l>
    K:=Ker[1]; l:=Ker[2]; g:=#K;
    if l eq 0 or l eq 1 then
      continue Ker;
    end if;
    gen:=K[1];
    Elist:=[Curve(g): g in gen];
    vprint AVIsogenies, 2: "! Source abelian variety", Elist;
    if Category(precomp) ne Assoc then
      if Elist ne last_Elist then // we need to update the precomputations for E
        _P0, tmp_precomp:=precompute_E(Elist: l2:=l2, init:=init);
      end if;
      last_Elist:=Elist;
    else // we assume the given precomp holds all the info we need
      tmp_precomp:=precomp;
    end if;

    key:=[l: i in [1..g]];
    if IsDefined(precompl, key) then
      tmp_precomp["l"]:=precompl[key];
      // Warning: sharing the precomputation of "l" this way only works if
      // D (ie the level n) is the same each time (but the source
      // abelian variety can change
    end if;

    theta_iso, pre:=GetIsogeny(l,K, tmp_precomp);

    Dl:=Universe(pre["l"]["gen"]); key:=Invariants(Dl);
    precompl[key]:=pre["l"];

    if Category(callback) eq UserProgram then
      Append(~r, callback(theta_iso, Ker, Index(Kerlist, Ker)));
    else
      Append(~r, theta_iso);
    end if;
  end for;
  return r;

end function;
