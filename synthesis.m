/*
 * Functions that generate a list of lattices
 */

// Isomorphism class of a R-lattice given as by a Z-basis ZB 

LatticeIsomorphismClass:=function(ZB,R)
  K:=NumberField(R);
  P:= PB(ZB);
  C:= [ Index(Integers(K), Order(x[1])): x in P ];
  I:= &*[ ideal< R | Basis(x[1]) > : x in P ];
  return C,I;
end function;

// compare two lattices and order them with respect to their number of components (>), if it is free or not (<), and if their are isomorphic or not (=)

ComparisonLattices:=function(L1,L2)
  R:=L1[4];
  c:=Conductor(R);
  Inv11:=L1[6];Inv12:=L1[7];
  Inv21:=L2[6];Inv22:=L2[7];
  g:=#Inv11;
  cList:=[c : i in [1..g]];
  if L1[5] gt L2[5] then
    return 1;
  end if;
  if L2[5] gt L1[5] then
    return -1;
  end if;
  if (Inv11 eq cList and IsPrincipal(Inv12)) and (Inv21 ne cList or 
    IsPrincipal(Inv22) eq false) then
    return -1;
  end if;
  if (Inv21 eq cList and IsPrincipal(Inv22)) and (Inv11 ne cList or 
    IsPrincipal(Inv12) eq false) then
    return 1;
  end if;	
  if Inv11 eq Inv21 and IsPrincipal(ColonIdeal(Inv12,Inv22)) then
    if #L1[3] lt #L2[3] then
      return -1;
    end if;
    if #L2[3] lt #L1[3] then
      return 1;
    else
      return 0;
    end if;
  end if;
  return -1;
end function; 

/*Given an imaginary quadratic order R or its discriminant and an integer g 
it returns the list of all (by default otherwise projective) unimodular 
(indecomposable by default) positive definite hermitian module of rank g over R with their 
automorphism group and the number of orthogonal components.
The last two arguments gives the isomorphism class of the lattice.
The list is sort by first the free lattices, then by groups of isomorphic lattices and 
finally the decomposable ones by number of components
If a callback is given, it is used to process the lattices */

OrderToLattices:=function(Disc,g:Projective:=false,Indecomposable:=true, PrintList:=false, callback:=false)
  if Category(Disc) eq RngQuad then
    R:=Disc; // accept an order as entry
  else
    O:=MaximalOrder(QuadraticField(SquareFree(Disc)));
    R:=sub<O | Integers()!Sqrt(Disc/Discriminant(O))>;
  end if;
  vprintf AVIsogenies, 2: "** Compute the list of lattices for disc=%o, g=%o **\n", Discriminant(R), g;
  if Projective then
    LList:=AllUnimodOrder(R,g :Indecomposable:=Indecomposable);
  else
    LList:=AllUnimodOrder2(R,g:Indecomposable:=Indecomposable);
  end if;
  LList2:=[];
  for L in LList do
    OrderList,SteinitzClass:=LatticeIsomorphismClass(L[1],R);
    Append(~LList2, < Rows(L[1]),L[2],L[3],R, L[4], OrderList,SteinitzClass >);
  end for;
  LList3:=Sort(LList2,ComparisonLattices);

  if Projective eq false and Indecomposable eq true then vprint AVIsogenies, 2: "=> There are",#LList,"indecomposable lattices"; end if;
  if Projective eq true and Indecomposable eq true then vprint AVIsogenies, 2: "=> There are",#LList,"indecomposable projective lattices"; end if;
  if Projective eq false and Indecomposable eq false then vprint AVIsogenies, 2: "=> There are",#LList,"lattices"; end if;
  if Projective eq true and Indecomposable eq false then vprint AVIsogenies, 2: "=> There are",#LList,"projective lattices"; end if;

  // Handle lattices
  if #LList3 ge 1 then
    i:=1; R:=LList3[1][4]; c:=Conductor(R); cList:=[c : i in [1..g]];
    Inv11:=cList; Inv12:=2*ideal<R | 1/2>; Inv1f:=Inv11;
  end if;
  r:=[* *];
  for L in LList3 do
    free:=false; Inv21:=L[6];Inv22:=L[7];
    if Inv21 ne Inv11 or IsPrincipal(ColonIdeal(Inv12,Inv22)) eq false then
      vprint AVIsogenies, 3: "*** New isomorphism class ***";
      Inv11:=Inv21;Inv12:=Inv22;
    end if;
    if Inv21 eq Inv1f and IsPrincipal(Inv22) then
      free:=true;
    end if;
    if L[5] eq 1 then
      if free then
        vprint AVIsogenies, 3: "- Indecomposable free lattice",i,"with #Aut=",#L[3]; 
      else
        vprint AVIsogenies, 3: "- Indecomposable lattice",i,"with #Aut=",#L[3];
      end if;
    else
      if free then
        vprint AVIsogenies, 3: "- Free lattice",i,"with #Aut=",#L[3]," and",L[5],"components";
      else
        vprint AVIsogenies, 3: "- Lattice",i,"with #Aut=",#L[3]," and",L[5],"components";
      end if;
    end if;
    if Category(callback) eq UserProgram then
      IndentPush();
      result:=callback(L);
      IndentPop();
      Append(~r, result);
    end if;
    i:=i+1;
  end for;
  if Category(callback) eq UserProgram then
    return r;
  else
    return LList3;
  end if;
end function;

// return the discriminant of the Frobenius endomorphism of an elliptic
// curve defined over a finite field. This is also the discriminant of the
// smallest order in its isogeny class
function PiDiscriminantOfEllipticCurve(E);
  m:=Trace(E); q:=#BaseRing(E); Disc:=m^2-4*q;
  return Disc;
end function;

/*Given an elliptic curve with endomorphism ring R:=Z[pi] and an integer g 
it returns the list of all (by default otherwise projective) unimodular 
(indecomposable by default) hermitian module of rank g over R. */

EllipticCurveToLattices:=function(E,g:Projective:=false,Indecomposable:=true, callback:=false)
  return OrderToLattices(PiDiscriminantOfEllipticCurve(E),g : Projective:=Projective,Indecomposable:=Indecomposable, callback:=callback);
end function;

/*
 * Functions that generate kernels from lattices
 */

Rescale2:=function(R,x,n)
  v:=Eltseq(x);
  return R!((v[1]-n*v[2])*R.1+v[2]*R.2);
end function;

//given an orthogonal family generating N and a lattice L this function returns a matrix R^2g->R^g which commutes with the inclusion L -> N.
PBtoINT:=function(A,L)
    ZB:=L[1];H:=L[2];R:=L[4];
  g:=Rank(H);
  K:=NumberField(R);
  H:=ChangeRing(H,K);
  r:=#ZB; //r counts the number of vectors to generate L (hence between g and 2g)
  Msurj:=Matrix(K,r,g,ZB);
return ChangeRing(Transpose(Conj(Transpose(A))*H*Transpose(Msurj)),R);
end function;


/*Given a Z-basis of a R=Z[pi]-lattice (ZB,H) and an elliptic curve E/Fq
returns the kernel of a (ell,...,ell) isogeny E^g->A when it exists. 
The parameter ellList control the degrees we try. We also impose ell to be prime to q
It can be a bound (by default 100) for the largest ell we allow. But we can also directly pass a list of ell to try.
We use a special convention to return <[], 0> when no isogeny is found, and
<[],1> when the lattice is completely decomposable
*/

LatticeToKernel:=function(L,E : ellList:=100)
  R:=L[4]; K<pi>:=NumberField(R); w:=R.2;
  q:=#BaseRing(E); m:=Trace(E); scale:=-Integers()!((Trace(w)-m)/2);
  SFC:=false; ellmin:=1;
  function callback(A, ell)
    if ell eq 0 then
      vprint AVIsogenies, 3: "-> no kernels found";
      return <[],ell>;
    elif ell eq 1 then
      vprint AVIsogenies, 3: "-> the lattice is completely decomposable";
      return <[],ell>;
    end if;
    P:=Transpose(PBtoINT(A,L));
    P:=Matrix(R,#Rows(P),#Rows(Transpose(P)),[[Rescale2(R,coeff,scale):coeff 
      in Eltseq(line)]:line in Rows(P)]);
    B:=EllipticBasisTorsion(E,ell);
    rL:=RepresentationOnTorsion(P,B : ord:=ell);
    Ka:=Kernel(rL);
    Isotropic,K:=BasisZn(Ka);
    if Isotropic then
      return <AbstractKernelToKernel(K,B),ell>;
    else
      return false;
    end if;
  end function;
  return mIdentity(L : qCond:=q, ellList:=ellList, callback:=callback);
end function;

/* same as before but treat several kernels at the same time */
LatticesToKernels:=function(LList,E : ellList := 100)
  return [* LatticeToKernel(L,E : ellList:=ellList) : L in LList *];
end function;

/* Given an elliptic curve E with endomorphism ring R:=Z[pi] and an integer 
g it returns a list of triple (Ker,ell,L) with Ker the kernel of a
(ell,...,ell) isogeny E^g->A for all principally polarized A in the isogeny
class of E^g. If ell=0 it means that it failed to do so (we keep it in the
list because even if we did not find a kernel the corresponding lattice may
be interesting; it will be filtered out in IsogeniesFromKernels).
One can ask for kernels which corresponds
to projective modules only (false by default), the varieties to be
indecomposable (true by default).
The program looks at ell smaller than a bound ellList (default 100)
*/

EllipticCurveToKernels:=function(E,g:Projective:=false,Indecomposable:=true, ellList:=100)
  Ep:=SimplifiedModel(E);
  callback:=function(L)
    ker:=LatticeToKernel(L,Ep: ellList:=ellList);
    vprint AVIsogenies, 3: "-> minimal ell=",ker[2];
    return <ker[1], ker[2], L>;
  end function;
  return EllipticCurveToLattices(Ep,g:Projective:=Projective,Indecomposable:=Indecomposable, callback:=callback);
end function;

// Given an order or a discriminant, find an elliptic curve with this exact
// order. The parameter q means we search over F_q. If q is negative it
// means we search E over all finite fields of odd size < -q.
// If maximal_curve is true (by default), we choose the twist with the
// maximal number of points
OrderToEllipticCurve:=function(Disc : q:=-100, maximal_curve:=true)
  vprint AVIsogenies, 1: "** Finding an elliptic curve with discriminant", Disc;
  if Category(Disc) eq RngQuad then // allow an order
    Disc:=Discriminant(Disc);
  end if;
  if q le 0 then
    Couples:=NonMaxOrd(Disc,-q);
    if Couples eq [] then
      vprint AVIsogenies, 2: "  ! No elliptic curves found over Fq with q odd for q<", -q;
      return false;
    end if;
    qi:=Couples[1];
    m:=Couples[2];
    E:=EllipticCurveWithMininalEndomorphismRing(m,qi);
  else
    b,m:=IsSquare(Disc+4*q);
    if b eq false then 
      vprint AVIsogenies, 2: "  ! No elliptic curves found over Fq for q=", q;
      return false;
    else
      E:=EllipticCurveWithMininalEndomorphismRing(-m,q);
    end if;
  end if;
  if maximal_curve then
    Elist:=Twists(E);
    max_card, index:=Max([#E: E in Elist]);
    E:=Elist[index];
  end if;
  return SimplifiedModel(E);
end function;

// Magic function that calls OrderToEllipticCurve if needed before EllipticCurveToKernels according to the type of the input
// This return the elliptic curve and list of kernels
GenerateKernels:=function(input,g : Projective:=false, Indecomposable:=true, q:=-100, ellList:=100)
  if Category(input) ne CrvEll then
    input:=OrderToEllipticCurve(input: q:=q);
    if Category(input) eq BoolElt then
      return input, [];
    end if;
    vprint AVIsogenies, 2: "Got E", input;
  end if;
  kernels:=EllipticCurveToKernels(input,g : Projective:=Projective,Indecomposable:=Indecomposable, ellList:=ellList);
  return input, kernels;
end function;

// Find a discriminant of a defect 0 elliptic curve
DiscriminantDefect0EllipticCurve:=function(q)
  m:=Floor(2*Sqrt(q));
  if Category(q) eq FldFin then
    q:=#q;
  end if;
  return m^2-4*q;
end function;
