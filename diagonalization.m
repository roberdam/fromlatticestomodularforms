Conj:=function(X)
  Y:=X;
  m:=Nrows(X);
  n:=Ncols(X);
  for i:=1 to m do
    for j:=1 to n do
      Y[i,j]:=ComplexConjugate(X[i,j]);
    end for;
  end for;
  return(Y);
end function;

SProduct:=function(X,Y,H)
  K:=Parent(Eltseq(X)[1]);
  H:=ChangeRing(H,K);
  g:=#Eltseq(X);
  U:=Conj(Matrix(K,1,g,Eltseq(X)));
  V:=Matrix(K,g,1,Eltseq(Y));
  return (U*H*V)[1,1];
end function;

function BackTrack(F,S,H)
  K:=BaseRing(H);
  g:=Rank(H);
  if #F eq g then
    return F;
  end if;
  if #F+#S lt g then
    return [];
  end if;
  v:=S[1];
  //assert forall{ u : u in F | SProduct(u,v,H) eq 0 };
  T:=BackTrack(Append(F,v), [s: s in S | SProduct(s,v,H) eq 0], H);
  if T ne [] then
    return T;
  end if;
  return BackTrack(F,S[2..#S],H);
end function;


/* given a R-lattice L  try to find an orthogonal basis
in the lattice with norm ell less than N (by default 50). If Odd:=true (by default) it 
tries to get ell odd. If qCond is not 1 (by default) try to find something prime to qCond.
We also can impose that ell is squarefree (by default we do not)*/



/* old version
mIdentity:=function(L : Odd:= true, Ellmin:=1, N:=50, qCond:=1, SquareFreeCond:=false)
    ZB:=L[1];H:=L[2];R:=L[4];
	g:=Rank(H);
	K:=NumberField(R);
	Kab:=AbelianExtension(K);
	Oab:=BaseRing(Kab);
	Det:=Determinant(H);
	if IsEven(g) and (not IsLocalNorm(Kab,Oab!Det)) then
    	print "no orthogonal family with same norm.";
    	return ZeroMatrix(Integers(),g,g),0;
	end if;
	if Odd and IsProj(ZB,R,2) eq false then
    	print "no orthogonal family with same odd norm.";
    	return ZeroMatrix(Integers(),g,g),0;
	end if;
	if Odd then
      	ell:=Ellmin;
      	incr:=2;
    else
      ell:=Ellmin;
      incr:=1;
    end if;
    MZB:=Matrix(ZB);
    S:=Eltseq(Rows(SolveNorm(MZB,H,ell)));
    T:=BackTrack([],S,H);
    while (Gcd(qCond,ell) ne 1 or (SquareFreeCond eq true and IsSquarefree(ell) eq false) or T eq []) do
   	   if ell gt N then
        	if Odd eq false and (IsOdd(g) or IsLocalNorm(Kab,Oab!Det)) then
				print "there is an ell larger than",N;
    			return ZeroMatrix(Integers(),g,g),0;
			end if;
			if Odd and IsOdd(g) and IsProj(ZB,R,2) and  IsLocalNorm(Kab,Oab!5,2) then
				print "there is an odd ell larger than",N;
    			return ZeroMatrix(Integers(),g,g),0;
			end if;	
			print "no ell found smaller than",N;
			return 	ZeroMatrix(Integers(),g,g),0;
      	else
        	ell +:= incr;
        	S:=Eltseq(Rows(SolveNorm(MZB,H,ell)));
        	T:=BackTrack([],S,H);
      	end if;
    end while;
    return Transpose(Matrix(K,g,g,T)),ell;
end function;
*/

function costOfOneEll(ell)
  u := SumOfSquares(ell);
  if #u eq 1 or #u eq 2 then
    return ell;
  else
    return ell^2;
  end if;
end function;

function compareIsogenyCost(l1, l2);
  c1:=costOfOneEll(l1);
  c2:=costOfOneEll(l2);
  if c1 lt c2 then
    return -1;
  elif c1 eq c2 then
    if l1 lt l2 then
      return -1;
    elif l1 gt l2 then
      return 1;
    else
      return 0;
    end if;
  else
    return 1;
  end if;
end function;

// Output a list of isogeny degrees to try, up to max, subject to some
// conditions
// There is no really a need to pass q, since mIdentity will filter out ell
// non prime to q already
function IsoDegrees(max: min:=1, Odd:=true, q:=1, SquareFreeCond:=false, SortByIsogenyTime := false)
  if Odd then
    list:=[i: i in [min..max] | IsOdd(i)];
  else
    list:=[i: i in [min..max]];
  end if;
  if SortByIsogenyTime then
    list:=[i: i in [min..max] | costOfOneEll(i) le max];
    list:=Sort(list, compareIsogenyCost);
  end if;
  return [i: i in list | Gcd(q,i) eq 1 and (not SquareFreeCond or IsSquarefree(i))];
end function;

// This try to find a diagonal orthogonal basis (of a given norm ell) for a given lattice, along with their common norm
// If none is found, return the zero matrix and 0
// If a callback is passed, it is used to filter the result. If the
// callback returns false then we try another ell in the list ellList
mIdentity:=function(L : ellList:=100, qCond:=1, callback:=false)
  ZB:=L[1];H:=L[2];R:=L[4]; MZB:=Matrix(ZB); 
  g:=Rank(H); K:=NumberField(R);
  Kab:=AbelianExtension(K); Oab:=BaseRing(Kab); Det:=Determinant(H);
  if IsEven(g) and (not IsLocalNorm(Kab,Oab!Det)) then
    vprint AVIsogenies, 3: "-> no orthogonal family with same norm";
    return ZeroMatrix(Integers(),g,g),0;
  end if;
  if Category(ellList) eq RngIntElt then
    if ellList ge 0 then
      ellList:=IsoDegrees(ellList);
    else // hack to sort by isogeny time
      ellList:=IsoDegrees(ellList: SortByIsogenyTime:=true);
    end if;
  else
  end if;
  ellList:=[l : l in ellList | Gcd(qCond,l) eq 1];
  if ExistsFamily(R,[* ZB,H,L[3] *],2) eq false then
    vprint AVIsogenies, 3:  "-> no orthogonal family with same odd norm, filtering odd ell from the list";
    ellList:=[l : l in ellList | IsEven(l)];
  end if;
  vprint AVIsogenies, 7: "! Trying to find isogenies of degrees", ellList;
  for ell in ellList do
    S:=Eltseq(Rows(SolveNorm(MZB,H,ell)));
    T:=BackTrack([],S,H);
    if T ne [] then
      mat:=Transpose(Matrix(K,g,g,T));
      if Category(callback) eq UserProgram then
        r:=callback(mat,ell);
        if Category(r) ne BoolElt then
          return r;
        end if;
      else
        return mat,ell;
      end if;
    end if;
  end for;
  vprint AVIsogenies, 3: "! Aborting: did not find an isogeny with ell in", ellList;
  mat:=ZeroMatrix(Integers(),g,g); ell:=0;
  if Category(callback) eq UserProgram then
    return callback(mat,ell);
  else
    return mat,ell;
  end if;
end function;
