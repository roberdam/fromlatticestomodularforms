Description
===========

`FromLatticesToModularForms` is a magma package which allows to

- span the isogeny class (of principally polarised abelian varieties) of a
  power of an elliptic curve by enumerating unimodular hermitian lattices
- compute the abelian variety `A` corresponding to a given lattice by
  exhibiting a kernel and an isogeny from `E^g` to `A`
- `A` is represented by its theta null point (of level 2 or 4) in such a
  way that we give an affine lift of the theta null point corresponding to
  the pushforward of the standard diagonal differential `dx/y` on `E^g`
- in particular one can evaluate rational modular forms on `A`
- in dimension `2` or `3` we also provide code to recognize when `A` is a
  Jacobian and if so to find the corresponding curve.

See the paper for more details.

Getting started
===============

~~~ sh
# clone the repository
git clone https://gitlab.inria.fr/roberdam/fromlatticestomodularforms
cd fromlatticestomodularforms

# initialize vendor directory
git submodule update --init

# test if everything works correctly
magma -s examples/totest.m
~~~

Examples
========

The `examples` folder include some examples on how the piece of codes work
together to generate the lattices, compute the associated abelian
varieties, and specific modular forms.

We also give examples of computing defect-0 curves in genus 2 and 3, and
how to compute Igusa's modular form in dimension 4.
